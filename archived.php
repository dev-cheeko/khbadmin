<?php 
$pageTitle = "Archived products";
include "./includes/header.php"; ?>

<div class="wrapper">

<?php include "./includes/topbar.php"; ?>
  
  <?php include "./includes/sidebar.php"; ?>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
      <?php 
      include_once("includes/breadcrumb.php");
      ?>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-12">
               <div class="card">
                   <div class="card-header">
                       <div class="d-flex justify-content-between">
                         <span>Products List</span>
                         <span><a href="add-product.php">Add Product</a></span>
                       </div>
                   </div>
                   <div class="card-body">
                        <button class="btn btn-danger mb-3 resorePrd">Restore selected</button>
                        <table style="width: 100%; font-size: .9rem;" class="table  table-striped table-condensed dataTable" id="archi__prdTable">
                            <thead>
                                <tr class="t-header">
                                    <th> <input type="checkbox" class='prdarchi_checkall' id='prdarchi_checkall'></th>
                                    <th>Image</th>
                                    <th>Product name</th>
                                    <th>Category name</th>
                                    <th>Status</th>
                                    <th>Quantity</th>
                                    <th>Created at</th>
                                    <th>Updated at</th>
                                    <th>Actions</th>
                                </tr>
                                <div class="archi_inptr"></div>
                            </thead>
                        </table>
                   </div>
               </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include "./includes/footer.php"; ?>