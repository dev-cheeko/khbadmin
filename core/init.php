<?php 
    // For Development Purpose //remove below lines before going to production
    error_reporting( E_ALL ); 
    ini_set('display_errors' , 1);

    ob_start();
    session_start();

    // Config file
    include "config/Config.php";
    
    // Vendors
    include dirname(__DIR__)."/vendor/autoload.php";

    // Including Database Class
    include "database/Database.php";

    // Including Function
    include "function/function.php";

    // Auto loading classes
    spl_autoload_register(function($classname){
        require __DIR__.DIRECTORY_SEPARATOR."class".DIRECTORY_SEPARATOR.$classname.".php";
    });

    // All Instantiation Goes Here
    $imageUploadClass   = new ImageUpload; // Image upload class 
    $login              = new Login; // Login class
    $users              = new Users; // Users class
    $categoryClass      = new Category; // Category class
    $productClass       = new Product; // Product class
    $tagClass           = new Tags; // Tag class
    $couponClass        = new Coupons; // Tag class
    $smsClass           = new Sms; // SMS class
    $widgetClass        = new Widget; // SMS class
     
    $orderClass         = new Orders;
    
    $invClass           = new Invoice;

    // $duration = new Duration();

?>