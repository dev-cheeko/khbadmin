<?php 

class Users extends Base {
    public function getUserDataByEmail( $email ) {

        $sql = "SELECT * FROM `users` WHERE `email` = :email";
        $stmt = $this->getdb()->dbh->prepare($sql);
        
        $stmt->bindParam(":email" , $email, PDO::PARAM_STR);
        $stmt->execute();

        if($stmt->rowCount() > 0) {
            return $stmt->fetch(PDO::FETCH_OBJ);

        } else {
            return false;
        }
    }


    public function getUserDataById( $id ) {
        $sql = "SELECT * FROM `users` WHERE `id` = :id ";
        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->bindParam(":id" , $id , PDO::PARAM_STR);
        $stmt->execute();

        if($stmt->rowCount() > 0) {
            return $stmt->fetch(PDO::FETCH_OBJ);
        } else {
            return false;
        }
    }

}

?>