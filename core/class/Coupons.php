<?php 

class Coupons extends Base {

    private $allCouponList = array();
    private $tableName = "coupons";

    public function getAllCoupons()
    {
        $coupons = $this->getData( $this->tableName );
        return $coupons;
    } 

    public function addCoupon($data) {
        
        $name = $data['_coupnname'];
        $disc = $data['_coupondiscount'];
        $exp = $data['_couponexp'];
        $sql = "INSERT INTO $this->tableName (`coupon_name` , `coupon_discount`  , `exp_time` ) VALUES(:coupon_name , :coupon_discount , :exp_time)";
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->bindParam(":coupon_name" , $name , PDO::PARAM_STR );
        $stmt->bindParam(":coupon_discount" , $disc , PDO::PARAM_INT );
        $stmt->bindParam(":exp_time" , $exp , PDO::PARAM_STR );

        if($stmt->execute()) {
        
            return true;
            
        } else {

            return false;
        
        }
    }


    // Get coupon data by id 
    public function getCouponDataById($id) 
    {
        $sql = "SELECT * FROM `$this->tableName` WHERE `id` = :id ";

        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->bindParam(":id" , $id , PDO::PARAM_INT);
        $stmt->execute();

        if($stmt->rowCount() > 0) {
            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
    }

    // Edit coupon
    public function editCoupon( $id , $data="" )
    {
        $cname = $data['cname'];
        $cdisc = $data['disc'];
        $expt  = $data['exp'];

        if(!empty($data) ) {
            
        }

        $sql = "UPDATE $this->tableName SET  
        `coupon_name`       = :cnmae ,  
        `coupon_discount`   = :cdisc ,
        `exp_time`          = :cexp ,
        WHERE `id`          = :cid   
        ";

        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->bindParam(":cname" , );
    }
}

?>