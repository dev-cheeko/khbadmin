<?php 

// Time and date humanizer
use Khill\Duration\Duration;
use Cocur\Slugify\Slugify;

class Orders extends Base {
    
    private $slug;
    private $timeHumanizer = null;

    // Tables 
    private $cartTable          = 'cart'; // cart table
    private $orderMasterAdress  = 'order_address_master'; // order master address
    private $orderMaster        = 'order_master'; // order master

    // Product class
    private $prdClass;

    public function __construct() {
        parent::__construct();
        $this->timeHumanizer = new Duration; 
        $this->slug     = new Slugify();   
        $this->prdClass = new Product;
    }

    // Orders executed
    public function productsDeliverd() {

        $sql = "SELECT * FROM $this->orderMaster WHERE `order_status` = '5' ";
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->execute();
        if( $stmt->rowCount() > 0 ){
            return $stmt->rowCount();
        }
        return 0;
    }



    // New orders by timestamp
    public function prdocutsNew(){
        
        $todayDate = date('Y-m-d H:i:s');
        $sql = "SELECT * FROM $this->cartTable WHERE ts = now() ";
        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->execute();
        if( $stmt->rowCount() > 0 ) {
            return $stmt->rowCount();
        }
        return 0;
    }

    public function productsPending() {

        $sql = "SELECT * FROM $this->orderMaster WHERE `order_status` = '1' ";
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->execute();
        if( $stmt->rowCount() > 0 ){
            return $stmt->rowCount();
        }
        return 0;
    }

    public function OrderDetailsWithProductsOrders($orderid) {
        $data = [];
        $orid = $orderid;
        $sql = "SELECT * FROM `$this->cartTable` WHERE order_id = :orid";
        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->bindParam(":orid" , $orid , PDO::PARAM_STR);
        $stmt->execute();
        if($stmt->rowCount() > 0) {
            $orderData = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $orderData;
        } else {
            return false;
        }
    }

    public function ordersMasterByOrderId($orderId) {
        $orid = $orderId;
        $sql = "SELECT * FROM $this->orderMaster WHERE `order_id` = :orid ";
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->bindParam(":orid" , $orid , PDO::PARAM_STR);
        $stmt->execute();
        if($stmt->rowCount() > 0) {
          return $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            return 'no';
        }
    }
    
    public function OrdersDatatable() 
    {
        $data = array();
        $requestData = $_REQUEST;
        $columns = array(
            // column index => database column name
            0 => 'id' , 
            1 => 'customer_id' , 
            2 => 'order_id' ,
            3 => 'txn_id' ,
            4 => 'status' ,
            5 => 'order_credit' ,
            6 => 'price' ,
            7 => ''
        );

        $sql    = "SELECT * FROM $this->orderMaster ";
        // $sql .= " ORDER BY `id` DESC";
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->execute();

        // Total Data
        $totalData = $stmt->rowCount();
        $totalFiltered = $totalData;

        $sql = "SELECT * " ;
        $sql .= " FROM $this->orderMaster WHERE 1=1";

        if(!empty($requestData['search']['value'])) {
            $sb = $requestData['search']['value'];
            $sql .= " AND ( order_id LIKE '" . $sb . "%' "; 
            $sql .= " OR customer_id LIKE '". $sb ."%' )";
        }

        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->execute();

        $totalFiltered = $stmt->rowCount();

        $sql .= " ORDER BY `id` DESC ";
        
        /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->execute();
        foreach ( $stmt->fetchAll(PDO::FETCH_OBJ) as $key => $orders) {

            $ndata = array();
            $ndata[] = "";
            $ndata[] = $orders->customer_id;
            $ndata[] = "<div class='rounded p-1 bg-warning text-green text-bold' style='cursor: pointer;' onclick='orderdet(\"".$orders->order_id."\")'>$orders->order_id</div>";

            // Getting order txnid
            $ndata[] = $orders->txn_id;
                        

            // Getting order status
            $orStatusDes = "";
            switch ($orders->order_status) {
                case '0':
                    $orStatusDes = "active";
                    break;
                case '1':
                    $orStatusDes = "Pending";
                    break;
                case '2':
                    $orStatusDes = "Packed";
                    break;
                case '3':
                    $orStatusDes = "Return";
                    break;
                case '4':
                    $orStatusDes = "Out for delivery";
                    break;
                case '5':
                    $orStatusDes = "Delivered";
                    break;
                
                default:
                    $orStatusDes = "active";
                    break;
            }

            $ndata[] = '<select class="form-control-sm" id="ordst" data-orid='.$orders->order_id.' name="ordst">
            <option value='.$orders->order_status.'>'.ucfirst($orStatusDes).'</option>
            <option value="1">Pending</option>
            <option value="3">Return</option>
            <option value="2">Packed</option>
            <option value="4">Out for delivery</option>
            <option value="5">Delivered</option>
            </select>';
            
            // Order Credit 
            $ndata[] = $orders->order_credit;
            $ndata[] =  $orders->order_payment_method;

            $ndata[] =  $orders->buy_amount;
            
            //timestamp
            // $ndata[] = '';
            
            $ndata[] = '<form action="invoice.php"  method="post"><input name="orid" hidden value='.$orders->order_id.'><div class="btn btn-group-sm">
            <button class="btn btn-warning" title="invoice"><i class="fas fa-file-alt"></i></button>
            </div></form>';

            $data[]  = $ndata;

        }

        $allDatas = array(
            "draw"              => intval($requestData['draw']) ,
            "data"              => $data ,
            "recordsTotal"      => intval($totalData),
            "recordsFiltered"   => intval($totalFiltered),
        );

        return $allDatas;
    }

    public function shippingAddress($oid) {
        $orid = $oid;
        $sql = "SELECT * FROM `$this->orderMasterAdress` WHERE `order_id` = :orid";
        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->bindParam(":orid" , $orid , PDO::PARAM_STR);
        $stmt->execute();
        if($stmt->rowCount() > 0) {
            $shipData = $stmt->fetch(PDO::FETCH_ASSOC);
            return $shipData;
        } else {
            return false;
        }
    }

    public function statusChange($oid , $status) {
        $orderid = $oid;
        $stVal = $status;
        $sql = "SELECT * FROM `$this->orderMaster` WHERE `order_id` = :orid";
        $stmt = $this->getdb()
                    ->dbh
                    ->prepare($sql);

        $stmt->bindParam(":orid" , $orderid, PDO::PARAM_STR);
        
        if($stmt->execute()){
            if($stmt->rowCount() > 0) {
                $sql = "UPDATE $this->orderMaster SET `order_status` = :stval WHERE `order_id` = :orid ";

                $stmt = $this->getdb()->dbh->prepare( $sql );
                $stmt->bindParam(
                    ":stval" , 
                    $stVal , 
                    PDO::PARAM_STR
                );
                $stmt->bindParam(
                    ":orid" , 
                    $orderid, 
                    PDO::PARAM_STR
                );

                if( $stmt->execute() ) {
                    return true;
                } 
            }
        } else {
            return false;
        }
    }

    public function orderInvoiceData($orderID){
        // Order Data
        $orderData = $this->ordersMasterByOrderId($orderID);

        // Product Data
        $productData = $this->OrderDetailsWithProductsOrders($orderID);

        // Shipping Data 
        $shipingData = $this->shippingAddress($orderID);

        $data = array(
            
            'shippingData' => [
                'purchaser_name'    => $shipingData['first_name'].' '.$shipingData['last_name'],
                'ship_address'      => $shipingData['street_address'].' '.$shipingData['city'].' '.$shipingData['state'].' '.$shipingData['pin_code'],
                'email'             => $shipingData['email_id'], 
                'mobile'            => $shipingData['mobile'], 
            ] ,
            'orderProductdata' => $orderData,
            'productData'      => $productData,
        );

        return $data;
    }
}
?>