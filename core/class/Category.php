<?php 

use Cocur\Slugify\Slugify;

class Category extends Base {
    
    // Category Name
    private $categoryName;

    // Table name
    private $tablName = "categories";
    private $ptablName = "parent_categories";

    // Slug
    private $slug; // slugify  

    public function __construct() {
        parent::__construct();
        $this->slug     = new Slugify(); 
    }


    public function editCategory( $cid , $parentCatId , $categoryName ) {
        $sql = "UPDATE `$this->tablName` SET `category_name` = :cname , `category_slug` = :cs , `parent_cat_id` = :pcid  WHERE `id` = :id ";
        $ctSlug = $this->slug->slugify($categoryName);
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->bindParam(":cname" , $categoryName , PDO::PARAM_STR); // category name
        $stmt->bindParam(":cs" , $ctSlug , PDO::PARAM_STR ); // category slug
        $stmt->bindParam(":pcid" , $parentCatId , PDO::PARAM_INT  ); // parent category id
        $stmt->bindParam(":id" , $cid , PDO::PARAM_INT  ); // parent category id
        
        if( $stmt->execute() ) {
            return true;
        } else {
            return false;
        }
    }


    private function allCategories() {
        
        $allCategories = $this->getData($this->tablName);

        if (count( $allCategories ) > 0) {
            return $allCategories;
        } else {
            return FALSE;
        }
    }

    public function checkParentClassAv($id) {
        $sql = "SELECT * FROM `$this->ptablName` WHERE `id` = :id ";
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->bindParam( ":id" , $id , PDO::PARAM_INT );
        $stmt->execute();
        if($stmt->rowCount() > 0)
        {
            return $stmt->fetch( PDO::FETCH_ASSOC );
        } else {
            return false;
        }
    } 
    
    private function addCategoryName($pcid , String $value) {
       
        // check if parentCat availiable or not
        $check = $this->checkParentClassAv($pcid);
        
        if( $check !== false ) {
            
            $sql = "INSERT INTO `$this->tablName` ( `category_name` , `category_slug` , `parent_cat_id`  )  ";
            $sql .= " VALUES ( :cname , :cs , :pcid ) ";
            
            $ctSlug= $this->slug->slugify( $value );
            $stmt = $this->getdb()->dbh->prepare( $sql );
            $stmt->bindParam(":cname" , $value , PDO::PARAM_STR); // category name
            $stmt->bindParam(":cs" , $ctSlug , PDO::PARAM_STR ); // category slug
            $stmt->bindParam(":pcid" , $pcid , PDO::PARAM_INT  ); // parent category id
            if( $stmt->execute() ) {
                return true;
            } else {
                return false;
            }
        
        }
    } 

    // Get All Categories
    public function getCategories() {
        return $this->allCategories();
    }

    public function setCategoryName($pcid , String $value){
        return $this->addCategoryName($pcid , $value);
    }

    // Get category name by id to save edited content
    public function getDataByID($id) 
    {
        return $this->fetchDataByID($this->tablName, $id); // return obj or false ( if not get any )   
    }


    public function getParentDataByID($id) 
    {
        $access = $this->checkAdminSession(); // checking for admin access
        if($access === TRUE) {
            return $this->fetchDataByID($this->ptablName, $id); // return obj or false (if not get any )
        } else {
            return $access; 
        }
    }

    // delCat 
    public function delCat($id) 
    {
        $access = $this->checkAdminSession(); // checking for admin access
        if($access === TRUE) {
            $check = $this->fetchDataByID($this->tablName , $id);
            if($check !== FALSE) {
                return $del = $this->delDataById($this->tablName, $id); // Return true or false  
            } else {
                return Constants::$alreadyDeleted;
            }
            return $del;
        } else {
            return $access;
        }
    }

    public function delParentCat($id) 
    {
        $access = $this->checkAdminSession(); // checking for admin access
        if($access === TRUE) {
            $check = $this->fetchDataByID($this->ptablName , $id);
            if($check !== FALSE) {
                return $del = $this->delDataById($this->ptablName, $id); // Return true or false  
            } else {
                return Constants::$alreadyDeleted;
            }
            return $del;
        } else {
            return $access;
        }
    }

    public function getParentCategoryByCatId ( $id ) {

        $sql = "SELECT * FROM `$this->ptablName` WHERE `id` = :id ";
        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->bindParam(":id" , $id , PDO::PARAM_INT);
        if($stmt->execute()) {
            if( $stmt->rowCount() > 0 ) {
                return $stmt->fetch(PDO::FETCH_ASSOC);
            }
        }

    }

    public function getParetCategories() {

        $p = $this->getData($this->ptablName);
        if (count( $p ) > 0) {
            return $p;
        } else {
            return FALSE;
        }
    }
    

}

?>