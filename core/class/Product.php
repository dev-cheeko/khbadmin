<?php 

// Time and date humanizer
use Khill\Duration\Duration;
use Cocur\Slugify\Slugify;

// Product Class
class Product extends Base {

    private $timeHumanizer = null;
    private $slug; // slugify 
    private $cat; // category class 
    private $coup; // coupon class

    public function __construct() {
        parent::__construct();
        $this->timeHumanizer = new Duration; 
        $this->slug     = new Slugify();   
        $this->cat = new Category;
        $this->coup = new Coupons;
    }

    // Table Name
    const tableName = "news"; 

    private $requiredProductFields = array(
        "title",
        "product_des",
        "slug",
        "product_category",
        "feature_img", 
    );

    private $requiredEditProductFields = array(
        "title",
        "product_des",
        "slug",
        "product_category",
    );

    public function addPrd(array $data) {

        $status = array();
        
        // News title
        $prd__title = $this->cleanString($data["product_details"]["title"]);

        $prd__slug = $this->cleanString($data["product_details"]["slug"]);
        
        // News description
        $prd__des = htmlspecialchars($data["product_details"]["product_des"]);
        
        // News Category
        $prd__category = $this->cleanString($data["product_details"]["product_category"]);
        
    
        // News feature image
        $prd__featureImg = $data["product_details"]["feature_img"]["name"]; 

        // News More Images 
        $prd__moreImg = $data["product_details"]["more_images"];
        
        if( !empty($prd__title) && 
            !empty($prd__category) && 
            !empty($prd__des) && 
           !empty($prd__featureImg) &&
           !empty($prd__slug)  
        ) {
            // Uploading Feature Image
            $uploadFImg = $this->imgUpload($data["product_details"]["feature_img"]);

            $uploadFImg = json_decode($uploadFImg , true);

            if($uploadFImg["status"] === 200) {
                $fileName = $uploadFImg["data"]["filename"];
            } 

            $sql = "INSERT INTO ".self::tableName." (
            `title` , 
            `description` ,
            `category_id` ,
            `imgUrl` ,
            `slug` 
            ) VALUES (
            :product_name ,
            :product_desc ,
            :product_categoryid ,
            :product_imgUrl ,
            :slug 
            )";

            $prd__slug = $this->slug->slugify($prd__slug);

            $stmt = $this->getdb()->dbh->prepare($sql);

            $stmt->bindParam(":product_name", $prd__title , PDO::PARAM_STR);
            
            $stmt->bindParam(":product_desc", $prd__des , PDO::PARAM_STR);
            
       
            
            $stmt->bindParam(":product_categoryid", $prd__category , PDO::PARAM_INT);

        

            $stmt->bindParam(":product_imgUrl", $fileName , PDO::PARAM_STR);
            
            
            $stmt->bindParam(":slug", $prd__slug , PDO::PARAM_STR);

            if($stmt->execute()) {

                $lastInsertId = $this->getdb()->dbh->lastInsertId();
              
                // Adding more images
                foreach ($prd__moreImg["name"] as $key => $img) {
                    if ( !empty($img) ) {
                       
                        $sql = "INSERT INTO `images` (`news_id` , `image_name` ,  `image_url` ) VALUES( :product_id , :image_name , :image_url  )";
    
                        $stmt = $this->getdb()->dbh->prepare( $sql );
    
                        foreach($prd__moreImg["name"] as $key => $value) {
    
                            $uploadMImg = $this->imgMultiUpload( $prd__moreImg );
                            
                            $fileNameM = $uploadMImg["filenames"][$key];
    
                            $stmt->bindParam( ":product_id" , $lastInsertId , PDO::PARAM_INT );
                
                            $stmt->bindParam( ":image_url" , $fileNameM , PDO::PARAM_STR );
    
                            $stmt->bindParam( ":image_name" , $prd__title , PDO::PARAM_STR );
                            
                            $stmt->execute();
                        
                        }
                        
                    }
                    break;
                }

                $status["status"] = 200;
                $status["success_msg"] = array (
                    "msg" => "Product successfully added",
                    "product_id" => $lastInsertId, 
                );
                return json_encode($status);
               

            } else {
                $status["status"] = 400;
                $status["error_msg"] = "Something went wrong while adding product please try again";
            }
        } else {

            $checkForEmptyFields = $this->checkForEmptyfields($data["product_details"] , $this->requiredProductFields);
            $status["status"] = 400;
            $status["error_msg"] = "Empty fields";
            $status["required_fields"] = $checkForEmptyFields;
            return json_encode($status);
        }

    }


    public function editPrd($id , array $data) {

        $status = array();
        
        // Product title
        $prd__title         = $this->cleanString($data["product_details"]["title"]);
        
        $prd__slug = $this->cleanString($data["product_details"]["slug"]);
        
        // Product description
        $prd__des           = htmlspecialchars($data["product_details"]["product_des"]);
        
        // Product Category
        $prd__category      = $this->cleanString($data["product_details"]["product_category"]);
        
      
     
        // Product feature image
        $prd__featureImg    = $data["product_details"]["feature_img"]["name"]; 

        // Product More Images 
        $prd__moreImg = $data["product_details"]["more_images"];
        
        if (
            !empty($prd__title) && 
            !empty($prd__category) &&
            !empty($prd__des) &&
            !empty($prd__slug)
        ) {

            // Uploading Feature Image
            if(!empty($prd__featureImg)) {

                $uploadFImg = $this->imgUpload($data["product_details"]["feature_img"]);
                $uploadFImg = json_decode($uploadFImg , true);
                if($uploadFImg["status"] === 200) {
                    $fileName = $uploadFImg["data"]["filename"];
                } 
            
            }
            

            $sql = "UPDATE  ".self::tableName." SET  
     
            `title` = :product_name ,
            `description` = :product_desc ,
            `category_id` = :product_categoryid ,";

            if(!empty($prd__featureImg)) {

                $sql .= " `imgUrl` = :product_imgUrl , ";
            }

            $sql .= " `news_available` = :is_avail ,
            `slug` = :slug WHERE `id` = :id ";
            $prd__slug = $this->slug->slugify($prd__slug);
            $stmt = $this->getdb()->dbh->prepare($sql);
            $stmt->bindParam(":product_name", $prd__title , PDO::PARAM_STR);
            $stmt->bindParam(":product_desc", $prd__des , PDO::PARAM_STR);
            $stmt->bindParam(":product_categoryid", $prd__category , PDO::PARAM_INT);
            if(!empty($prd__featureImg)) {
                $stmt->bindParam(":product_imgUrl", $fileName , PDO::PARAM_STR);
            }
            $stmt->bindParam(":is_avail", $prd__isAvail , PDO::PARAM_STR);
            $stmt->bindParam(":slug", $prd__slug , PDO::PARAM_STR);
            $stmt->bindParam(":id", $id , PDO::PARAM_INT);

            if($stmt->execute()) {

                $lastInsertId = $id;
                
            

                // Adding more images
                foreach ($prd__moreImg["name"] as $key => $img) {
                    if ( !empty($img) ) {
                       
                        $sql = "INSERT INTO `images` (`news_id` , `image_name` ,  `image_url` ) VALUES( :product_id , :image_name , :image_url  )";
    
                        $stmt = $this->getdb()->dbh->prepare( $sql );
    
                        foreach($prd__moreImg["name"] as $key => $value) {
    
                            $uploadMImg = $this->imgMultiUpload( $prd__moreImg );
                            
                            $fileNameM = $uploadMImg["filenames"][$key];
    
                            $stmt->bindParam( ":product_id" , $lastInsertId , PDO::PARAM_INT );
                
                            $stmt->bindParam( ":image_url" , $fileNameM , PDO::PARAM_STR );
    
                            $stmt->bindParam( ":image_name" , $prd__title , PDO::PARAM_STR );
                            
                            $stmt->execute();
                        
                        }
                        
                    }
                    break;
                }

                $status["status"] = 200;
                $status["success_msg"] = array (
                    "msg" => "Product successfully updated",
                    "product_id" => $lastInsertId, 
                );
                return json_encode($status);
               

            } else {
                $status["status"] = 400;
                $status["error_msg"] = "Something went wrong while adding product please try again";
                return json_encode($status);
            }
            
        } else {

            $checkForEmptyFields = $this->checkForEmptyfields($data["product_details"] , $this->requiredEditProductFields);
            $status["status"] = 400;
            $status["error_msg"] = "Empty fields";
            $status["required_fields"] = $checkForEmptyFields;
            return json_encode($status);
        }

    }

    // get all products in datatable format
    public function dataTable() {
        
        $data = array();

        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index => database column name
            0 => 'imgUrl',
            1 => 'title',
            2 => 'category_name',
            3 => 'status',
            4 => 'created_at',
            5 => 'updated_at',
            6 => 'actions',
            
        );

       // getting total number records without any search
        $sql = "SELECT * ";
        $sql .= " FROM news WHERE archive = '0'";
        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->execute();

        // Total Data
        $totalData = $stmt->rowCount();

        // when there is no search parameter then total number rows = total number filtered rows.
        $totalFiltered = $totalData;

        $sql = "SELECT * " ;
        $sql .= " FROM news WHERE 1=1 AND archive = '0'" ;
        if (!empty($requestData['search']['value'])) { 
            $sv= $requestData['search']['value'];
            // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( title LIKE '%" . $sv . "%' ";
        }

        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->execute();
        $totalFiltered = $stmt->rowCount();

        $sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "  ";
        /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */
        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->execute();
        foreach ($stmt->fetchAll(PDO::FETCH_OBJ) as $key => $prd) {

            $getcategoryName = $this->fetchDataByID('categories', $prd->category_id);
            if($getcategoryName == FALSE) {
                $categoryName = "No category assigned";    
            } else {
                $categoryName = $getcategoryName->category_name;
            }

            $ndata = array();
            $ndata[] = "<input type='checkbox' class='prddelete_check' id='delcheck_".$prd->id."' onclick='prdcheckbox();' value='".$prd->id."'>";
            $ndata[] = "<img src='".$prd->imgUrl."' class='img-fluid' width='50px'>";
            // Product Name
            $ndata[] = $prd->title;
            // Category Name
            $ndata[] = $categoryName;
            // Status
            $ndata[] = ($prd->news_available == '1') ? '<button onclick="prdStatus('.$prd->id.')" data-prd-id="'.$prd->id.'" class="btn btn-block btn-success btn-sm prd-status">Active</button>' : '<button data-prd-id="'.$prd->id.'" onclick="prdStatus('.$prd->id.')"class="btn btn-block btn-danger btn-sm prd-status">Deactive</button>';
          
            // $ndata[] = $this->timeHumanizer->humanize($this->extratctTimeFromTimstamp( $prd->ts ));
            // $ndata[] = $this->timeHumanizer->humanize($this->extratctTimeFromTimstamp( $prd->ts ));
          
            $ndata[] = $prd->created_at ;
            $ndata[] = $prd->updated_at ;

            $ndata[] = "<a class='btn btn-warning btn-sm' href='edit.php?type=product&id=".base64_encode($prd->id)."' data-id='".$prd->id."' class='mr-2' title='edit'><i class='fa fa-edit'></i></a>
            <button data-prd-id='".$prd->id."' title='Delete' onclick='prdDel(".$prd->id.")' class='btn btn-danger btn-sm prd-del'><i class='fa fa-trash'></i></button>";
            $ndata["DT_RowId"] = intval($prd->id);

            $data[] = $ndata;
           
        }

        $allDatas = array (
            "draw"              => intval($requestData['draw']), 
            "data"              => $data,
            "recordsTotal"      => intval($totalData),
            "recordsFiltered"   => intval($totalFiltered),
        );

        return $allDatas;
    }

    // get all products in datatable format
    public function archivedProductDataTable() {
        
        $data = array();

        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index => database column name
            0 => 'product_imgUrl',
            1 => 'product_name',
            2 => 'category_name',
            3 => 'status',
            4 => 'quantity',
            5 => 'created_at',
            6 => 'updated_at',
            7 => 'actions',
        );

       // getting total number records without any search
        $sql = "SELECT * ";
        $sql .= " FROM products WHERE archive = '1' ";
        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->execute();

        // Total Data
        $totalData = $stmt->rowCount();

        // when there is no search parameter then total number rows = total number filtered rows.
        $totalFiltered = $totalData;

        $sql = "SELECT * " ;
        $sql .= " FROM products WHERE 1=1 AND archive = '1'" ;
        if (!empty($requestData['search']['value'])) { 
            $sv= $requestData['search']['value'];
            // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql .= " AND ( product_name LIKE '%" . $sv . "%' ";
            $sql .= " OR retail_price LIKE '" . $sv . "%' ) ";
        }

        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->execute();
        $totalFiltered = $stmt->rowCount();

        $sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "  ";
        /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */
        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->execute();
        foreach ($stmt->fetchAll(PDO::FETCH_OBJ) as $key => $prd) {

            $getcategoryName = $this->fetchDataByID('categories', $prd->category_id);
            if($getcategoryName == FALSE) {
                $categoryName = "No category assigned";    
            } else {
                $categoryName = $getcategoryName->category_name;
            }

            $ndata = array();

            $ndata[] = "<input type='checkbox' class='prdrestore_check' id='restorecheck_".$prd->product_id."' onclick='prdrestorebox();' value='".$prd->product_id."'>";
            $ndata[] = "<img src='".$prd->product_imgUrl."' class='img-fluid' width='50px'>";
            // Product Name
            $ndata[] = $prd->product_name;
            // Category Name
            $ndata[] = $categoryName;
            // Status
            $ndata[] = ($prd->news_available == '1') ? '<button onclick="prdStatus('.$prd->product_id.')" data-prd-id="'.$prd->product_id.'" class="btn btn-block btn-success btn-sm prd-status">Active</button>' : '<button data-prd-id="'.$prd->product_id.'" onclick="prdStatus('.$prd->product_id.')"class="btn btn-block btn-danger btn-sm prd-status">Deactive</button>';
         
            // $ndata[] = $this->timeHumanizer->humanize($this->extratctTimeFromTimstamp( $prd->ts ));
            // $ndata[] = $this->timeHumanizer->humanize($this->extratctTimeFromTimstamp( $prd->ts ));
            $ndata[] = '';
            $ndata = '';

            $ndata[] = "<a class='btn btn-warning btn-sm' href='edit.php?type=product&id=".base64_encode($prd->product_id)."' data-id='".$prd->product_id."' class='mr-2' title='edit'><i class='fa fa-edit'></i></a>";
            $ndata["DT_RowId"] = intval($prd->product_id);
            $data[] = $ndata;
           
        }

        $allDatas = array (
            "draw"              => intval($requestData['draw']), 
            "data"              => $data,
            "recordsTotal"      => intval($totalData),
            "recordsFiltered"   => intval($totalFiltered),
        );

        return $allDatas;
    }

    // Latest 10 Products 
    public function latestWidgetProducts() {
        $sql = "SELECT * FROM ".self::tableName." ORDER BY `product_id` DESC LIMIT  10 ";
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->execute();
        if($stmt->rowCount() > 0 ) {
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            return false;
        }
    }


    public function statusCheckerProduct($id) {

        $sql = "SELECT `news_available` FROM  ".self::tableName." WHERE `id` = :prdid";
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->bindParam(":prdid" , $id , PDO::PARAM_STR);
        if($stmt->execute()) {
    
            if($stmt->rowCount() > 0) {

                $data = $stmt->fetch(PDO::FETCH_ASSOC);
                if($data['news_available'] == '1') {

                    return 'activated';
                }
                if($data['news_available'] == '0') {

                    return 'deactivated';
                }
            }
        
        } else {
        
            return false;
        
        }
    
    }

    // Change status of product availablity 
    public function statusChange($id) {

        // check for if product activated or not
        
        $statusCh = $this->statusCheckerProduct($id);

        if($statusCh == 'deactivated') {
            
            $sql = "UPDATE ".self::tableName." SET news_available = '1'  WHERE id = :id ";
            $stmt = $this->getdb()->dbh->prepare( $sql );
    
            $stmt->bindParam(":id" , $id , PDO::PARAM_STR);
            
            if( $stmt->execute() ) {
                return 'activated';
            } 
        }

        if($statusCh == 'activated') {
            
            $sql = "UPDATE ".self::tableName." SET news_available = '0'  WHERE id = :id ";
            $stmt = $this->getdb()->dbh->prepare( $sql );
            $stmt->bindParam(":id" , $id , PDO::PARAM_STR);
            
            if( $stmt->execute() ) {
                return 'deactivated';
            } 
        }


    }

    // Setting product archive value to 1 to delete 
    public function delproduct($id) {
    
        $sql = "UPDATE ".self::tableName." SET  `archive` = '1'  WHERE `id` = :id ";
        $stmt =  $this->getdb()->dbh->prepare( $sql );
        $stmt->bindParam(":id" , $id , PDO::PARAM_STR);
        if($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    
    }


    // Setting product archive value to 1 to delete 
    public function restoreProduct( $restoreid ) {
    
        $sql = "UPDATE ".self::tableName." SET `news_available` = '1'  WHERE `id` = :id ";
        $stmt =  $this->getdb()->dbh->prepare( $sql );
        $stmt->bindParam(":id" , $restoreid , PDO::PARAM_STR);
        if($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    
    }

    // Check product availiable or not 
    /**
     *  productCheckAv function
     * @param [int] $id for product id
     * @param string $slug for slug column if param avai
     * @param array $data if another data ava
     * @return mixed
     */
    public function productCheckAv($id , $slug='' , array $data=array() ) {

        $sql = "SELECT * FROM ".self::tableName."  ";
        $sql .= "WHERE `id` = :id ";
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->bindParam(":id" ,  $id, PDO::PARAM_INT);
        
        if(!empty($slug)) {
            $sql .=  " AND `slug` = :slug  ";
            $stmt->bindParam(":slug" ,  $slug, PDO::PARAM_STR);
        }

        if($stmt->execute()) {
            if($stmt->rowCount() > 0) {
                return $stmt->fetch(PDO::FETCH_ASSOC);
            } else {
                return false;
            }
        }
    }

    public function productExtraImages ($id) 
    {
        $sql    = "SELECT * FROM `images` WHERE `id`  = $id ";
        $stmt   = $this->getdb()->dbh->prepare( $sql );
        $stmt->execute();
        if($stmt->rowCount() > 0 ) {
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            return '';
        }
    }

    public function productData($id) {
        $productData = $this->productCheckAv( $id );
        if($productData !== false) {
            $prdID = $productData['id'];
            $prdCategoryID = $productData['category_id'];
            // product category
            $categoryData = $this->cat->getDataByID( $prdCategoryID );
            
            // product parent category


            // Product extra images
            $prdImages = $this->productExtraImages($prdID);
            $data = array (
                "product_data" => $productData,
                'categorydata' => $categoryData,
                "extraimages" => $prdImages,
            );
            return $data;
        }
    }
    
}
?>