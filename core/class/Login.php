<?php 


class Login extends Base {

    private $id;
    private $email;
    private $user_role;

    private $userClass;
    
    public function __construct() {
        $this->userClass = new Users();
    }
    

    private function setCredSession() {
        $_SESSION['id'] = $this->id;
        $_SESSION['email'] = $this->email;
        $_SESSION['user_role'] = $this->user_role;
    }
    // Login checker
    public function accLogin($email , $pass) 
    {

        $email = stringClear($email);
        $pass = stringClear($pass);

        if(!empty($email) || !empty($pass)) {
            
            $userData = $this->userClass->getUserDataByEmail( $email );
            if(!$userData === false || !empty($userData)) {

                if(password_verify($pass , $userData->password)) {
                    
                    $this->id = $userData->id;
                    $this->email = $userData->email;
                    $this->user_role = $userData->user_role;

                    $this->setCredSession();
                }else {
                    return msg('alert-danger' , Constants::$invCred);
                }
            }else {
                return msg('alert-danger' , Constants::$noUser);
            }
        
        } else {
            return msg('alert-danger' , Constants::$requiredField);
        }
    }

}
?>