<?php 

class Constants {
    public static $requiredField = "Fields are required";
    public static $invalidEmail = "Invalid Email";
    public static $invCred = "Invalid credentials";
    public static $noUser = "No user exist";
    
   
    public static $defaultError = "Something went wrong";
    
    public static $exist = "Already exist";
   
    public static $alreadyDeleted = "already deleted";


    
    /**
     *  Invalid image extension
    */
    // Invalid image extension
    public static $invalidImage = "invalidImage"; 
    public static $invalidImgFile = "Please select - jpeg, jpg , png";
   
    // Invalid image Size
    public static $invalidImageSize = "invalidImageSize";
    public static $invalidImgSize = "Cannot upload more than 2 mb file";
   
    // Success fullyuploaded
    public static $uploadedSuccesFully = "uploadedSuccesFully";
    public static $imgUploadedSuccess = "Sucessfully uploaded";

    // Permissions
    public static $permissionDenied = "permission denied";
}


?>