<?php 
use Konekt\PdfInvoice\InvoicePrinter;
class Invoice extends Base {

    private $inv;

    public function __construct() {
        parent::__construct();
        $this->inv = new InvoicePrinter();
    }


    public function invPrint($orderId) {

        /* Header settings */
        $this->inv->setLogo("dist/img/u-logo.png");   //logo image path
        $this->inv->setColor("#007fff");      // pdf color scheme
        $this->inv->setType("Sale Invoice");    // Invoice Type
        $this->inv->setReference("INV-55033645");   // Reference
        $this->inv->setDate(date('M dS ,Y',time()));   //Billing Date
        $this->inv->setTime(date('h:i:s A',time()));   //Billing Time
        $this->inv->setDue(date('M dS ,Y',strtotime('+3 months')));    
        // Due Date
        $this->inv->setFrom(array("Seller Name","Sample Company Name","128 AA Juanita Ave","Glendora , CA 91740"));
        $this->inv->setTo(array("Purchaser Name","Sample Company Name","128 AA Juanita Ave","Glendora , CA 91740"));
        
        $this->inv->addItem("AMD Athlon X2DC-7450","2.4GHz/1GB/160GB/SMP-DVD/VB",6,0,580,0,3480);
        $this->inv->addItem("PDC-E5300","2.6GHz/1GB/320GB/SMP-DVD/FDD/VB",4,0,645,0,2580);
        $this->inv->addItem('LG 18.5" WLCD',"",10,0,230,0,2300);
        $this->inv->addItem("HP LaserJet 5200","",1,0,1100,0,1100);
        
        $this->inv->addTotal("Total",9460);
        $this->inv->addTotal("VAT 21%",1986.6);
        $this->inv->addTotal("Total due",11446.6,true);
        
        $this->inv->addBadge("Payment Paid");
        
        $this->inv->addTitle("Important Notice");
        
        $this->inv->addParagraph("No item will be replaced or refunded if you don't have the invoice with you.");
        
        $this->inv->setFooternote("My Company Name Here");
        
        $this->inv->render('example1.pdf','D'); 
        /* I => Display on browser, D => Force Download, F => local path save, S => return document as string */
    }


}
?>