<?php 
/***
 * Base Class 
 */
// This class will be extended to all class which depends on database 



class Base  {
    private $db;
    
    // Upload Dir
    const uploadDir = UPLOAD_FOLDER."/";

    public function __construct() {
      
        $this->db       = new Database;
    }

    // Getting $db Property and returning db instantition
    public function getdb() {
        return $db = $this->db;
    }

    // Hash Pass
    public function hashPass($val) {
        return password_hash($val, PASSWORD_DEFAULT);
    }

    // Clean strings
    public function cleanString(string $v) {
        
        $string = trim($v);
        $string = htmlentities($v);
        $string = htmlspecialchars($v);
        $string = strip_tags($v);
        $string = stripslashes($v);
        $string = stripslashes($v);
    
        return $string;
    }

    // Check Session
    public function checkAdminSession() 
    {
        $tableName = "users";
        $colName = "user_role";
        $userSessionID = $_SESSION['id'];
        
        $sql = "SELECT `user_role` FROM `users` WHERE `id` = :id ";
        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->bindParam(":id" , $userSessionID , PDO::PARAM_INT);
        $stmt->execute();
        
        if( $stmt->rowCount() > 0 ) {
            $userRole  = $stmt->fetch(PDO::FETCH_OBJ);

            if($userRole->user_role === "0") {
                return TRUE;
            } else {
                return Constants::$permissionDenied;
            }
        }
    }

    // Common methods====================================================================
    public function extratctTimeFromTimstamp( $ts ) {
        $time=substr($ts, strpos($ts , " "));
        return $time;
    }

    // Common SQL Methods===========================================

    // Get data
    public function getData(String $tbname) {
        $sql = "SELECT * FROM `$tbname` ";
        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    // Add Single Data
    public function addDataSingle(String $tableName, String $colName , String $value) {
        $sql = "INSERT INTO `$tableName` (`$colName`) VALUES(:$colName)";
        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->bindParam(":$colName" , $value, PDO::PARAM_STR);
        if($stmt->execute() === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Check existed Single column
    public function checkExistedSingleData( String $tableName, String $colName , String $value ){
        $sql = "SELECT `$colName` FROM `$tableName` WHERE `$colName` = :$colName ";
        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->bindValue(":$colName" , $value);
        $stmt->execute();

        if($stmt->rowCount() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // fetchDataByID
    public function fetchDataByID($tableName , $id) 
    {   
        $sql = "SELECT * FROM `$tableName`  WHERE `id` = :id ";
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->bindParam(":id" , $id, PDO::PARAM_INT);
        $stmt->execute();

        if($stmt->rowCount() > 0) {
            return $stmt->fetch(PDO::FETCH_OBJ);
        } else {
            return FALSE;
        }
    
    }

    // Delete By Id 
    public function delDataById($tablName, $id)
    {
        $sql = "DELETE FROM `$tablName` WHERE `id` = :id ";
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->bindParam(":id" , $id , PDO::PARAM_INT);
        if($stmt->execute()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Check for empty fields 
    public function checkForEmptyFields ($fieldsArray , $requiredFields) 
    {   
        $emptyFields = array();
        foreach ($fieldsArray as $key => $value) {
            if(in_array($key , $requiredFields)) {
                if(empty($fieldsArray[$key])) {
                    $emptyFields[] = $key;
                }
            }
        }
        return $emptyFields;
    }

    // Insert method
    /**
     * This function insert data into table it require fields in array 
    */
    public function dataInsert($tablename , array $fields) {

        $dataBind = "";
        $col = "";

        $sql = "INSERT INTO `$tablename` ( ";

        foreach($fields as $key => $value) {
            $col .= $key." , " ;
            $dataBind .= ":".$key." , ";
        }
        $sql .=  rtrim($col, ' ,')." ) VALUES( ";
        $sql .= rtrim($dataBind, ' ,')."  )";

        $stmt = $this->getdb()->dbh->prepare( $sql );
        
        foreach($fields as $key=> $value ) {
            $stmt->bindParam(":".$key , $value);
        }

        if($stmt->execute()) 
        {
            return true;
        } else {
            return false;
        }

    }
    

    // Image upload function / method
    public function imgUpload( $imgData ) {

        // New File Name
        $nameComb = random_int(0,9999999999999)."_".date("m-d-y-h-i-s-a" , time())."-";

        // Current File Name
        $fileName = strtolower(pathinfo($imgData['name'], PATHINFO_FILENAME));

        // Extension Name
        $extension = pathinfo($imgData['name'], PATHINFO_EXTENSION);
        
        //New File name of current filename with extension 
        $newFilename = self::uploadDir.$nameComb.$fileName.".".$extension;

        // Temporary Filename
        $tempFileName = $imgData["tmp_name"];

        // Move uploaded File
        $moved = move_uploaded_file($tempFileName , dirname(dirname(__DIR__))."/".$newFilename);

        if($moved) {
            $status = array(
                "status" => 200,
                "data" => [
                    "filename" => $newFilename
                ]
            );

            return json_encode($status);

        } else {

            $status = array(

                "status" => 400,
                "msg" => "please check your directory settings or image filename or image size we support only 
                jpeg, jpg, png greater than 500kb and less than 1mb please note the larger the size the slow the internet will take to load to image.
                ",
            );

            return json_encode($status);
        }
    }

    public function imgMultiUpload( $imgData ) {
        $status = array( "status" => "", "filenames" => array() );
        foreach ( $imgData['name'] as $key => $value ) {
            // New File Name
            $nameComb = random_int(0,9999999999999)."_".date("m-d-y-h-i-s-a" , time())."-";

            // Current File Name
            $fileName = strtolower(pathinfo($imgData['name'][$key], PATHINFO_FILENAME));

            // Extension Name
            $extension = pathinfo($imgData['name'][$key], PATHINFO_EXTENSION);

            //New File name of current filename with extension 
            $newFilename = self::uploadDir.$nameComb.$fileName.".".$extension;

            // Temporary Filename
            $tempFileName = $imgData["tmp_name"][$key];
            // Move uploaded File
            $moved = move_uploaded_file($tempFileName , dirname(dirname(__DIR__))."/".$newFilename);
            if( $moved ) {

                $status["status"] = 200;
    
            } else {
    
                $status["status"] = 400;
            }

            $status["filenames"][] = $newFilename;
        }

        return $status;
    }
}

?>