<?php 

// Widget Clss 

class Widget extends Base {

    // TABLES 
    private $prdTable           = 'products';
    private $catTable           = 'categories';
    private $parent_catTable    = 'parent_category';
    private $couponsTable       = 'coupons';
    private $frontUsersTable    = 'front_users';

    
    public function __construct() {
        parent::__construct();
    }




    // getting all the products
    public function allProducts() {
        $sql = "SELECT * FROM ".$this->prdTable." ";
        $stmt = $this->getdb()->dbh->prepare($sql);
        $stmt->execute();
        
        if($stmt->rowCount() > 0) {
            
            return $stmt->fetchAll( PDO::FETCH_ASSOC );
        } else {
            return false;
        }

    }


    public function allActivatedFrontUsers() {

        $sql = "SELECT * FROM $this->frontUsersTable WHERE `verified` = '1' ";
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->execute();
        if($stmt->rowCount() > 0) {   
            return $stmt->fetchAll( PDO::FETCH_ASSOC );
        } else {
            return false;
        }
    }

    // Coupons  created
    public function allCouponsCreated() {
        $sql = "SELECT * FROM $this->couponsTable ";
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->execute();
        if($stmt->rowCount() > 0) {   
            return $stmt->fetchAll( PDO::FETCH_ASSOC );
        } else {
            return false;
        }
    }

    // All archived products
    public function allArchivedProducts() {
        $sql = "SELECT * FROM $this->prdTable WHERE `archive` = '1'";
        $stmt = $this->getdb()->dbh->prepare( $sql );
        $stmt->execute();
        if($stmt->rowCount() > 0) {   
            return $stmt->fetchAll( PDO::FETCH_ASSOC );
        } else {
            return false;
        }
    }
}


?>