<?php 

class ImageUpload  {
    // Upload Dir
    const uploadDir = UPLOAD_FOLDER."/";


    // Image upload function / method
    public function imgUpload( $imgData ) {

        // New File Name
        $nameComb = date("m-d-y-h-i-s-a" , time())."-";

        // Current File Name
        $fileName = strtolower(pathinfo($imgData['name'], PATHINFO_FILENAME));

        // Extension Name
        $extension = pathinfo($imgData['name'], PATHINFO_EXTENSION);
        
        //New File name of current filename with extension 
        $newFilename = self::uploadDir.$nameComb.$fileName.".".$extension;

        // Temporary Filename
        $tempFileName = $imgData["tmp_name"];

        // Move uploaded File
        $moved = move_uploaded_file($tempFileName , dirname(dirname(__DIR__))."/".$newFilename);

        if($moved) {
            $status = array(
                "status" => 200,
                "data" => [
                    "filename" => $newFilename
                ]
            );

            return json_encode($status);

        } else {

            $status = array(

                "status" => 400,
                "msg" => "please check your directory settings or image filename or image size we support only 
                jpeg, jpg, png greater than 500kb and less than 1mb please note the larger the size the slow the internet will take to load to image.
                ",
            );

            return json_encode($status);
        }
    }
}

?>