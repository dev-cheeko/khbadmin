<?php 

function stringClear( $string ) {

    $string = trim($string);
    $string = htmlentities($string);
    $string = htmlspecialchars($string);
    $string = strip_tags($string);
    $string = stripslashes($string);
    $string = stripslashes($string);

    return $string;

}

function extratctTimeFromTimstamp( $ts ) {
    $time=substr($ts, strpos($ts , " "));
    return $time;
}


// Bootstrap Alerts
function msg( $class , $msg ) {
    echo "<div class='alert-$class' alert-dismissible fade show' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
            <span class='sr-only'>Close</span>
        </button>
        $msg
    </div>";
}

// Toastr Alerts

// Success
function toastrSuccess($value) {
    echo "<script>
    toastr.success('$value' , 'Successful');
    </script>";
}
// Warning
function toastrWarning($value) {
    echo "<script>
    toastr.warning('$value' , 'Warning');
    </script>";
}
// Info
function toastrInfo($value) {
    echo "<script>
    toastr.info('$value' , 'Info');
    </script>";
}
// Error
function toastrError($value) {
    echo "<script>
    toastr.error('$value' , 'Error');
    </script>";
}

// Refresh
function refresh($location , $ts="1500") {
    
    echo "<script>
    setTimeout(function(){
        window.location.href= '".$location."';
    }, ".$ts.");
    </script>";

}

// server refresh // redirection funcion after some time
function sRefresh($location , $ts="3") //default time is 3 in seconds
{
    header("refresh: $ts; url = $location");
}

// Check for empty fields 
function checkForEmptyFields ($fieldsArray , $requiredFields) 
{   
    $emptyFields = array();
    foreach ($fieldsArray as $key => $value) {
        if(in_array($key , $requiredFields)) {
            if(empty($fieldsArray[$key])) {
                $emptyFields[] = $key;
            }
        }
    }
    return $emptyFields;
}

?>