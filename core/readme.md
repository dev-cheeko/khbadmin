# PHP INITIAL CORE FILES

## Included Folders
Ajax 
Class
Config
Database
Function
Plugins

## Ajax Folder
All  ajax files goes here

## Class Folder
This folder holds all the classes

## Config Folder
This Folder holds all the configuration

## Database Folder
This Folder holds Databse class and connection

## Function Folder
This Function Folder holds Function class

## Plugins
This Folder holds all the plugins

```
Init File binds all the folder in one file and atuoload the classes and instantiation of classes

```

I made this initial setup because i love php and want to way deeper.

Config folder holds all the database or any type of configuration like database configuration or email configuration or base url , app url etc


This setup will change as i learn or go deeper in php.



## To connect to database in classes you just have to extend base class and use getdb()->db.

```
public function x() {
    $sql = "---------"; // sql statement
    $stmt = $this->getdb()->dbh->prepare($sql);
    // more code 
}
```

If you want to use public folder and put any public files you can make public folder outside of root folder and create .htaccess File and put this code in .htaccess file 
```
<IfModule mod_rewrite.c>
Options -Multiviews
RewriteEngine On
RewriteBase /rohit/public
RewriteCond %{REQUEST_FILENAME}% !-d
RewriteCond %{REQUEST_FILENAME}% !-f
RewriteRule ^(.+)$ index.php?url=$1 [QSA,L]
</IfModule>
```

change rohit to desired location 
```
RewriteBase /rohit/public
```


and also create index.php file in public folder and this code
```
<?php require_once "../core/init.php"; ?>
```

and make .htaccess file in root folder (outside of public) and add this code 
```
<IfModule mod_rewrite.c>
RewriteEngine on
RewriteRule ^$ public/ [L]
RewriteRule (.*) public/$1 [L]
</IfModule>
```