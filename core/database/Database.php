<?php 
/** 
 * Database connection using PDO
*/

class Database {

    private $host   = HOSTNAME;     //hostname ( value coming from config file)
    private $dbname = DBNAME;      //dbname ( value coming from config file)
    private $dbuser = DBUSER;     //username ( value coming from config file)
    private $dbpwd  = DBPWD;     // password ( value coming from config file)


    public $dbh;
    private $error;

    public function __construct() {

        $dsn = 'mysql:host='.$this->host.';dbname=' .$this->dbname;

        $options = array(

            PDO::ATTR_PERSISTENT =>  true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        
        );

        try {
            $this->dbh = new PDO($dsn , $this->dbuser , $this->dbpwd , $options);
        } catch (PDOException $th) {
            $this->error = $th->getMessage();
            echo $this->error;
        }

    }

}

?>