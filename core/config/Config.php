<?php 
// TimeZone 
date_default_timezone_set("Asia/Kolkata");

/**
 *  Database Configuration Goes here
 *  Hostname 
 *  Database name
 *  Username
 *  Password
*/
define("HOSTNAME","localhost");
define("DBNAME","khb_core");
define("DBUSER","root");
define("DBPWD","");


// App Root
define("APPROOT" , dirname(__DIR__));

// Base URL
// define("URLROOT" , "http://localhost/app-user-management/"); // Put your domain url here dev

define("URLROOT" , "http://bookyourproduct.com/"); // Put your domain url here prod env


// Sitename
define("SITENAME" , "Khabariya News Portal");

// App name
define("APPNAME" , "Khabariya news");

// Upload Folder

// Production
// $uploadFolderDir =  '../uploads';
// define("UPLOAD_FOLDER" , $uploadFolderDir);

// Development
define("UPLOAD_FOLDER" , "uploads");

?>