<?php 
$pageTitle = "SMS / EMAIL ";
include "./includes/header.php"; ?>

<div class="wrapper">

<?php include "./includes/topbar.php"; ?>
  
  <?php include "./includes/sidebar.php"; ?>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
      <?php 
       include_once("includes/breadcrumb.php");
       ?>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-12">
               <div class="card">
                   <div class="card-header">
                       <div class="d-flex justify-content-between">
                         <span>Send Sms</span>
                       </div>
                   </div>
                   <div class="card-body bs">
                        <?php
                          include './includes/components/sendsms/Comp-sendsms.php'; 
                        ?>
                   </div>
               </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include "./includes/footer.php"; ?>