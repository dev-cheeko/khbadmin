<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">

<!-- Brand Logo -->
<a href="index.php" class="brand-link">
  <img src="dist/img/u-logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
       style="opacity: .8">
  <span class="brand-text"><?php  echo APPNAME; ?></span>
</a>

<!-- Sidebar -->
<div class="sidebar">
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      
      <li class="nav-item has-treeview ">
        <a href="#" class="nav-link active">
          <i class="nav-icon fas fa-box"></i>
          <p>
            News
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="products.php" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>News List</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="add-product.php" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Add News</p>
            </a>
          </li>
        </ul>
      </li>

      <li class="nav-item has-treeview ">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-bezier-curve"></i>
          <p>
            Categories
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="category.php?type=parent" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Parent category</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="category.php?type=sub-category" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Sub category</p>
            </a>
          </li>
        </ul>
      </li>

      <!-- Extras -->
      <li class="nav-item has-treeview ">
        <a href="#" class="nav-link ">
          <i class="nav-icon fas fa-sliders-h"></i>
          <p>
            Extras
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="sms.php" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Send message</p>
            </a>
          </li>
        </ul>
      </li>

    </ul>
  </nav>
  <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>
