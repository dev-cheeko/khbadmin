<div class="time-section mx-auto d-flex align-items-center">
    <i class="far fa-clock text-white mr-1"></i>
    <p class="m-0 text-white timer"></p>
</div>