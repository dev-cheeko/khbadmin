<?php 
include "./core/init.php"; 
if(!$_SESSION['id']) {
  header("Location: login.php?invalidLogin");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?php echo isset($pageTitle) ? $pageTitle : ""; ?></title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Custom css -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
  <link rel="stylesheet" href="plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css">
  <link rel="stylesheet" href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.css">
  <link rel="stylesheet" href="plugins/datatables-select/css/select.bootstrap4.min.css">
  <link rel="stylesheet" href="dist/css/custom.css">
  
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
<!-- REQUIRED SCRIPTS -->

  <!-- jQuery -->
  <script src="plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
  <script src="plugins/datatables-select/js/dataTables.select.min.js"></script>
  <script src="plugins/datatables-select/js/select.bootstrap4.min.js"></script>
  <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
  <script src="plugins/typeahead.bundle.js"></script>
  
  <script src="vendor/tinymce/tinymce/jquery.tinymce.min.js"></script>
  <script src="vendor/tinymce/tinymce/tinymce.min.js"></script>
  <script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
  <script src="plugins/toastr/toastr.min.js"></script>
  <script src="plugins/moment/moment-with-locales.min.js"></script>

  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.min.js"></script>
</head>
<body class="hold-transition sidebar-mini">