<?php 
    // Checking for form submission
    if(isset($_POST['send__sms'])) {
        if($_POST['sms__number'] !== "" && !empty($_POST['sms__number'])) { 
            
            $number = $_POST['sms__number'];
            
            $msg = $_POST['sms__body'];

            if(strlen($number) > 10 || strlen($number) < 10 ) {
                toastrError( $value="Invalid number" );
            } else {

    
                $send = $smsClass->sendSms( $number , $msg );
                if($send == FALSE) {
                    toastrError( $value="Error sending sms" );
    
                } else {
                
                    switch ($send) {
    
                        case $send['responseCode'] == 'SUCCESS':
                            toastrSuccess("Message delivered successfully");
                            break;

                        case $send['responseCode'] == 'REQUIRED';
                            toastrError( $value="Message is required" );
                            break;

                        default:
                            # code...
                            break;
                    
                    }
                }
            }
        } else {
            toastrError( $value="Number is requird" );
        }
    }
?>
<form action="" method="post">
    <div class="row">
        <div class="col-md-8 m-auto">
            <form action="" method="post">
                <div class="form-group">
                    <label for="sms__number">Numbers</label>
                    <input  placeholder="Number..." type="text" name="sms__number" id="sms__number" class="form-control" required>
                    <small class="form-text text-muted">You can add multiple numbers ex(123456789 , 123456789 ...)</small>
                </div>
                <div class="form-group">
                    <label for="sms__body">Message</label>
                    <textarea required name="sms__body" cols="30" rows="10" id="sms__body" class="form-control" placeholder="Message.."></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" value="Send" name="send__sms" class="btn btn-primary btn-block btn-lg">
                </div>
            </form>
        </div>
    </div>
</form>