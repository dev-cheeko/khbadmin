<?php
    // Category Lists
    $categoryLists = $categoryClass->getCategories();

    // Add Products
    if(isset($_POST["__addprd"])) {

        $data = array();
        
        // All Products Details with feature img and more images
        $data["product_details"] = $_POST; // all post 

        $data["product_details"]["feature_img"] = $_FILES["product_featureimg"]; // product feature image
        $data["product_details"]["more_images"] = $_FILES["product_images"]; // product images


        $addPrd = $productClass->addPrd($data);

        $addPrd = json_decode($addPrd , true);

        switch ($addPrd["status"]) {
            case 200:
                toastrSuccess($addPrd["success_msg"]["msg"]);
                break;
            case 400:
                toastrError($addPrd["error_msg"]);
                echo "<style>";
                foreach ($addPrd["required_fields"] as $key => $value) {
                    echo "#$value {
                        border-color: red !important;
                    }";
                    if($value == "product_category") {
                        echo ".tox-tinymce {
                            border-color: red !important;
                        }";         
                    }
                }
                echo "</style>";
                break;
            default:
                # code...
                break;
        }

    }
?>
<form action="" enctype="multipart/form-data" method="POST" >
    <div class="form-group">
        <label for="title">News Title</label>
        <input type="text" name="title" id="title" value="<?php echo isset($_POST['title']) ? $_POST['title'] : ""; ?>" class="form-control" placeholder="News title">
        <small class="form-text text-muted">Please provide news title </small>
    </div>
    <div class="form-group">
        <label for="title">News Slug</label>
        <input type="text" name="slug" id="slug" value="<?php echo isset($_POST['slug']) ? $_POST['slug'] : ""; ?>" class="form-control" placeholder="News slug">
        <small class="form-text text-muted">Please provide news slug in english </small>
    </div>

    <div class="form-group">
        <label for="product_des">News Description</label>
        <textarea name="product_des" id="product_des" value="<?php echo isset($_POST['product_des']) ? $_POST['product_des'] : ""; ?>" class="form-control" cols="30" rows="10" placeholder="Product Description"></textarea>
        <small class="form-text text-muted">Please provide news description</small>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
            <label for="product_category">Category</label>
                <?php 
                if($categoryLists !== FALSE) {
                    echo "<select name='product_category' class='form-control' id='product_category'>
                    <option value=''>Select Category</option>"
                    ?>
                    <?php 
                        foreach($categoryLists as $key=>$value) {
                            echo "<option value='".$value->id."'>".$value->category_name."</option>";
                        }
                    ?>
                    </select>
                    <?php
                } else {
                    echo "<a class='' href='category.php'>Add category</a>";
                }
                ?>
                <small class="form-text text-muted">Please choose news category</small>
            </div>
        </div>
    </div>

    <div class="images border">
        <div class="row py-3">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="product_featureimg">Featured Image</label>
                    <input type="file" name="product_featureimg" id="f_img">
                    <small class="form-text text-muted">Please choose product featured image</small>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="product_images">Product Images</label>
                    <input type="file" name="product_images[]" id="product_images" multiple="multiple">
                    <small class="form-text text-muted">Please choose multple product images</small>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group my-3">
        <button class="btn btn-primary" name="__addprd" type="submit" >Add News</button>
    </div>
</form>