<?php 
    $categoryLists = $categoryClass->getCategories();

    if ( isset( $_GET['id'] ) ) { 
        $id = base64_decode($_GET['id']);
        $productAv = $productClass->productData( $id );  
        // print_r($productAv);      
    
        if (isset($_POST['__editprd'])) {
            $data = array();
            // All Products Details with feature img and more images
            $data["product_details"] = $_POST; // all post 

            $data["product_details"]["feature_img"] = $_FILES["product_featureimg"]; // product feature image
            $data["product_details"]["more_images"] = $_FILES["product_images"]; // product images

            $editPrd = $productClass->editPrd( $id , $data);
            $editPrd = json_decode($editPrd , true);
            switch ($editPrd["status"]) {
                case 200:
                    toastrSuccess($editPrd["success_msg"]["msg"]."Please refresh the browser");
                    break;
                case 400:
                    toastrError($editPrd["error_msg"]);
                    echo "<style>";
                    foreach ($editPrd["required_fields"] as $key => $value) {
                        echo "#$value {
                            border-color: red !important;
                        }";
                        if($value == "product_category") {
                            echo ".tox-tinymce {
                                border-color: red !important;
                            }";         
                        }
                    }
                    echo "</style>";
                    break;
                default:
                    # code...
                    break;
            }
        }

    }
?>

<div class="row">
    <div class="col-md-12">
        <form action="" enctype="multipart/form-data" method="POST" >
            <div class="form-group">
                <label for="title">News name</label>
                <input type="text" name="title" id="title" value="<?php echo isset($productAv['product_data']['title']) ? $productAv['product_data']['title'] : ""; ?>" class="form-control" placeholder="News title">
                <small class="form-text text-muted">Please provide news name </small>
            </div>

            <div class="form-group">
                <label for="title">News name</label>
                <input type="text" name="slug" id="slug" value="<?php echo isset($productAv['product_data']['slug']) ? $productAv['product_data']['slug'] : ""; ?>" class="form-control" placeholder="News Title">
                <small class="form-text text-muted">Please provide news slug in english </small>
            </div>

            <div class="form-group">
                <label for="product_des">News Description</label>
                <textarea name="product_des" id="product_des" value="<?php echo isset($_POST['description']) ? $_POST['description'] : ""; ?>" class="form-control" cols="30" rows="10" placeholder="News Description"><?php echo isset($productAv['product_data']['description']) ? html_entity_decode($productAv['product_data']['description']) : ""; ?></textarea>
                <small class="form-text text-muted">Please provide product description</small>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                    <label for="product_category">Category</label>
                        <?php 
                        if($categoryLists !== FALSE) {
                            echo "<select name='product_category' class='form-control' id='product_category'>
                            <option value='".$productAv['categorydata']->id."'>".$productAv['categorydata']->category_name."</option>"
                            ?>
                            <?php 
                                foreach($categoryLists as $key=>$value) {
                                    echo "<option value='".$value->id."'>".$value->category_name."</option>";
                                }
                            ?>
                            </select>
                            <?php
                        } else {
                            echo "<a class='' href='category.php'>Add category</a>";
                        }
                        ?>
                        <small class="form-text text-muted">Please choose news category</small>
                    </div>
                </div>
            </div>

            <div class="images border">
                <div class="row py-3">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="product_featureimg">Featured Image</label>
                            <input type="file" name="product_featureimg" id="f_img">
                            <small class="form-text text-muted">Please choose product featured image </small>
                        </div>
    <img src="<?php echo $productAv['product_data']['imgUrl']; ?>" width="50" class="img-fluid" alt="">
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="product_images">News Images</label>
                            <input type="file" name="product_images[]" id="product_images" multiple="multiple">
                            <small class="form-text text-muted">Please choose multple product images</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group my-3">
                <button class="btn btn-primary" name="__editprd" type="submit" >Edit product</button>
            </div>
        </form>
    </div>
</div>