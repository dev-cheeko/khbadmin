
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
      <?php 
       include_once("includes/breadcrumb.php");
       ?>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-12">
               <div class="card">
                   <div class="card-header">
                       <div class="d-flex justify-content-between">
                         <span>Add Category</span>
                         <span><a href="category.php?type=sub-category">Sub category</a></span>
                       </div>
                   </div>
                   <div class="card-body bs">
                   <div class="row">
                        <div class="col-md-6">
                            <?php 
                            $new_query_string = http_build_query($_GET);
                                //Add Category 
                                if(isset($_POST['_addCat'])) {
                                    
                                    if(empty($_POST['_category'])) {
                                        toastrError("Cannot add empty category");
                                    } else {
                                        $categoryName = stringClear($_POST['_category']);
                                        $onSubmit = $categoryClass->setCategoryName($categoryName);
                                        if($onSubmit === Constants::$exist) {
                                            toastrWarning("$categoryName category ".Constants::$exist);
                                        } else {
                                            toastrSuccess("$categoryName category successfull created");
                                        }
                                }
                                }

                                // Edit
                                if(isset($_GET['editcat'])) {
                                    $editcat__ID = stringClear($_GET['editcat']);
                                    $editcat__ID = intval($editcat__ID);
                                    $editData = $categoryClass->getParentDataByID($editcat__ID);
                                    
                                    if($editData === Constants::$permissionDenied) {
                                        toastrError(Constants::$permissionDenied);
                                    } else if($editData === FALSE) {
                                        toastrError("No category found");
                                    } else if(isset($editData->parent_cat_name)) {
                                        $editDataCategoryName = $editData->parent_cat_name;
                                    }
                                }

                                // Delete category
                                if(isset($_GET['delcat'])) {
                                    $delcat__ID = stringClear($_GET['delcat']);
                                    $delcat__ID = intval($delcat__ID);
                                    $delCat = $categoryClass->delParentCat($delcat__ID);
                                    if($delCat === Constants::$permissionDenied) {
                                        toastrError(Constants::$permissionDenied);
                                    } else if($delCat === FALSE) {
                                        toastrError("Something went wrong");
                                    } else if($delCat === TRUE) {
                                        toastrSuccess("Category deleted successfully");
                                    } else if($delCat === Constants::$alreadyDeleted) {
                                        toastrError("Category ".Constants::$alreadyDeleted);
                                    }
                                    
                                }
                            ?>

                            <form action="" method="post">
                                <div class="form-group">
                                    <label for="_category">Parent Category</label>
                                    <input required type="text" name="_category" id="_category" class="form-control" placeholder="Category name" value="<?php if(isset($editDataCategoryName)){ echo $editData->parent_cat_name;} ?>">
                                    <small class="form-text text-muted">Please type Category name</small>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit" name="_addCat" >Add category</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-bordered table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Parent category Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    $data = $categoryClass->getParetCategories();
                                    if($data !== FALSE) {
                                        foreach($data as $key => $value) {
                                    
                                ?>
                                    <tr>
                                        <td><?php echo $value->id; ?></td>
                                        <td><?php echo ucfirst($value->parent_cat_name); ?></td>
                                        <td>
                                        <a href="category.php?<?php echo $new_query_string; ?>&editcat=<?php echo $value->id; ?>" class="mr-2" title="edit"><i class="fa fa-edit"></i></a>
                                        <a href="category.php?<?php echo $new_query_string; ?>&delcat=<?php echo $value->id; ?>" class="text-danger" title="delete"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                   </div>
               </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->