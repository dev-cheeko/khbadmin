<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
      <?php 
       include_once("includes/breadcrumb.php");
       ?>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-12">
               <div class="card">
                   <div class="card-header">
                       <div class="d-flex justify-content-between">
                         <span>Add Category</span>
                         <div>
                            <span><a href="category.php?type=sub-category">Add Category</a></span>
                            &nbsp;
                            <span><a href="category.php?type=parent">Parent Category</a></span>
                         </div>
                       </div>
                   </div>
                   <div class="card-body bs">
                   <div class="row">
    <div class="col-md-6">
        <?php 
        $parentCategory = $categoryClass->getParetCategories();
        $new_query_string = http_build_query($_GET );
            //Add Category 
            if(isset($_POST['_addCat'])) {
                
                if(empty($_POST['_category']) || empty($_POST['_pcategory'])) {
                    
                    toastrError("Cannot add empty category");
                
                } else {

                    $parentCatId = $_POST['_pcategory'];    
                    $categoryName = stringClear($_POST['_category']);

                    $onSubmit = $categoryClass->setCategoryName( $parentCatId , $categoryName );
                    if($onSubmit == true)
                    {
                        toastrSuccess("Category added successfully!");
                    }
                    if($onSubmit == false)
                    {
                        toastrError("Something went wrong!");
                    }

                }
            }

            // Edit
            if(isset($_GET['editcat'])) {
                $editcat__ID = stringClear($_GET['editcat']);
                $editcat__ID = intval($editcat__ID);
                $editData    = $categoryClass->getDataByID($editcat__ID);
                // parent category data 
                $parentCategoryData = $categoryClass->checkParentClassAv($editData->parent_cat_id);

                if($editData === FALSE) {
                    toastrError("No category found");
                } else if(isset($editData->category_name)) {
                    $editDataCategoryName = $editData->category_name;
                } 
                if( isset($_POST['_editCat']) ) {

                    $parentCatId = $_POST['_pcategory'];    
                    $categoryName = stringClear($_POST['_category']);
                    $onSubmit = $categoryClass->editCategory($editcat__ID ,  $parentCatId , $categoryName );
                    if($onSubmit == true)
                    {
                        toastrSuccess("Category updated successfully!");
                    }
                    if($onSubmit == false)
                    {
                        toastrError("Something went wrong!");
                    }
                    
                }

            }

            // Delete category
            if(isset($_GET['delcat'])) {
                $delcat__ID = stringClear($_GET['delcat']);
                $delcat__ID = intval($delcat__ID);
                $delCat = $categoryClass->delCat($delcat__ID);
                if($delCat === Constants::$permissionDenied) {
                    toastrError(Constants::$permissionDenied);
                } else if($delCat === FALSE) {
                    toastrError("Something went wrong");
                } else if($delCat === TRUE) {
                    toastrSuccess("Category deleted successfully");
                } else if($delCat === Constants::$alreadyDeleted) {
                    toastrError("Category ".Constants::$alreadyDeleted);
                }
                
            }
        ?>

    <form action="" method="post">
            <div class="form-group">
                <label for="_pcategory">Parent category</label>
                <select name="_pcategory" id="_pcategory" class="form-control">
                <?php 
                if(isset($_GET['editcat'])) {
                    echo '<option value="'.$parentCategoryData['id'].'">'.$parentCategoryData['parent_cat_name'].'</option>';
                }else {
                    echo '<option value="">Select parent category</option>';
                }
                foreach ($parentCategory as $key => $pc) {
                ?>
                <option value="<?php echo $pc->id; ?>"><?php echo $pc->parent_cat_name; ?></option>
                <?php }
                ?>
                </select>
                <small class="form-text text-muted">Please assign parent category</small>
            </div>
            <div class="form-group">
                <label for="_category">Category</label>
                <input required type="text" name="_category" id="_category" class="form-control" placeholder="Category name" value="<?php if(isset($editDataCategoryName)){ echo $editData->category_name;} ?>">
                <small class="form-text text-muted">Please type Category name</small>
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit" name="_<?php if( isset($_GET['editcat']) ) { echo 'edit';}else{echo'add';} ?>Cat" ><?php if( isset($_GET['editcat']) ) { echo 'Edit';}else{echo'Add';} ?> category</button>
            </div>
        </form>
    </div>
    <div class="col-md-6">
        <table class="table table-bordered table-striped table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Category Name</th>
                    <th>Parent Category</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            <?php 
                $data = $categoryClass->getCategories();
                if($data !== FALSE) {
                    foreach($data as $key => $value) {
                ?>
                <tr>
                    <td><?php echo $value->id; ?></td>
                    <td><?php echo $value->category_name; ?></td>
                    <td>
                    <?php 
                    $parentCt = $categoryClass->getParentCategoryByCatId ( $value->parent_cat_id );
                    echo $parentCt['parent_cat_name'];
                    ?>
                    </td>
                    <td>
<a href="category.php?type=sub-category&editcat=<?php echo $value->id; ?>" class="mr-2" title="edit"><i class="fa fa-edit"></i></a>
                    <a href="category.php?<?php echo $new_query_string; ?>&delcat=<?php echo $value->id; ?>" class="text-danger" title="delete"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php 
            } } 
            ?>
            </tbody>
        </table>
    </div>
</div>
                   </div>
               </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->