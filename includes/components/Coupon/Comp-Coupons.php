<div class="row">
    <div class="col-md-6">
    <?php 
        if(isset($_POST['_addCoupon'])) {
            if(!empty($_POST['_coupnname']) || !empty($_POST['_coupondiscount'])) {
                $insCoup = $couponClass->addCoupon($_POST);

            }
        }

        // DELETE
        if(isset($_GET['delete'])) {

        }

        // EDIT
        // if(isset($_GET['edit'])) {
        //     $editid = $_GET['edit'];
        //     if( !empty($_GET['edit']) ) {
        //         $editCoup = $couponClass->editCoupon( $editid );
        //     }
        // }
    ?>
        <form action="" method="post">

            <div class="form-group">
                <label for="_coupnname">Coupon name</label>
                <input required type="text" name="_coupnname" id="_coupnname" class="form-control"
                    placeholder="Coupon name" value="<?php if(isset($editDataCouponName)){ echo $editDataCouponName;} 
                ?>">
                <small class="form-text text-muted">Please type Coupon name (ex: UTG10)</small>
            </div>

            <div class="form-group">
                <label for="_coupondiscount">Coupon discount</label>
                <input required type="text" name="_coupondiscount" id="_coupondiscount" class="form-control"
                    placeholder="Coupon discount"
                    value="<?php if(isset($editDataCouponDisc)){ echo trim($editDataCouponDisc);} ?>">
                <small class="form-text text-muted">Please type discount in percentage (do not include % sign) the
                    default is 0 if not filled </small>
            </div>

            <div class="form-group">
                <label for="_couponexp">Coupon expiration</label>
                <input type="date" name="_couponexp" id="_couponexp" class="form-control">
                <small class="form-text text-muted">Choose the expiration date of coupon</small>
            </div>

            <div class="form-group">
                <button class="btn btn-primary" type="submit"
                    name="_<?php if(!isset($_GET["editcop"])){ echo "add";} else{echo "edit";} ?>Coupon"><?php if(!isset($_GET["editcop"])){ echo "Add";} else{echo "Edit";} ?>
                    coupon</button>
            </div>
        </form>
    </div>
    <div class="col-md-6">
        <table class="table table-bordered table-striped table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Coupon Name</th>
                    <th>Discount</th>
                    <th>Exp Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php 
            $c = $couponClass->getAllCoupons();
            foreach ($c as $key => $v) {
                ?>
                <tr>
                    <td><?php echo $v->id; ?></td>
                    <td><?php echo $v->coupon_name; ?></td>
                    <td><?php echo $v->coupon_discount. ' %'; ?></td>
                    <td><?php echo $v->exp_time; ?></td>
                    <td>
                        <a class='btn btn-warning btn-sm'
                            href='coupons.php?edit=<?php echo $v->id; ?>' data-id='' class='mr-2' title='edit'>
                            <i class='fa fa-edit'></i>
                        </a>
                        <a class='btn btn-danger btn-sm'
                            href='coupons.php?delete=<?php echo $v->id; ?>' data-id='' class='mr-2' title='delete'>
                            <i class='fa fa-trash'></i>
                        </a>
                        
                    </td>
            <?php
                }
            ?>
            </tbody>
        </table>
    </div>
</div>