<div class="card">
    <div class="card-header">
        Latest products (Showing <?php echo  $productClass->latestWidgetProducts() !== false ?  count($productClass->latestWidgetProducts()) : '0'; ?> products)  
    </div>
    <div class="card-body">
    <table class="table table-responsive-sm table-striped">
    <thead>
        <tr class="t-header">
            <th>Image</th>
            <th>Product name</th>
            <th>Created at</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        
        $latestProducts = $productClass->latestWidgetProducts();
        if($latestProducts !== false) {
            foreach ($latestProducts as $key => $prod) {
                ?>
                <tr>
                    <td><img src="<?php echo $prod['product_imgUrl']; ?>" width="50" alt=""></td>
                    <td><?php echo $prod['product_name']; ?></td>
                    <td><?php echo $prod['ts']; ?></td>
                    <td>Show</td>
                </tr>
           <?php  }
        } else {
            echo 'No results';
        }
        ?>  

    </tbody>
    </table>
    </div>
</div>