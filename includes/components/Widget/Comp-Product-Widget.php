<div class="row">
    <div class="col-md-3 col-sm-6 col-12">
    <div class="info-box">
        <span class="info-box-icon bg-info"><i class="fas fa-box"></i></span>

        <div class="info-box-content">
        <span class="info-box-text">Products</span>
        <span class="info-box-number"><?php echo ($widgetClass->allProducts()) !== false ?  count($widgetClass->allProducts()) : '0';  ?></span>
        </div>
        <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
    <div class="info-box">
        <span class="info-box-icon bg-success">
        <i class="fas fa-box"></i></span>

        <div class="info-box-content">
        <span class="info-box-text">Sold Products</span>
        <span class="info-box-number">0</span>
        </div>
        <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
    <div class="info-box">
        <span class="info-box-icon bg-warning"><i class="fas fa-shopping-cart"></i></span>

        <div class="info-box-content">
        <span class="info-box-text">New orders</span>
        <span class="info-box-number">
        <?php echo $orderClass->prdocutsNew(); ?>
        </span>
        </div>
        <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
    <div class="info-box">
        <span class="info-box-icon bg-danger"><i class="fas fa-shopping-cart"></i></span>

        <div class="info-box-content">
        <span class="info-box-text">Orders pending</span>
        <span class="info-box-number">
            <?php echo $orderClass->productsDeliverd(); ?>
        </span>
        </div>
        <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
    </div>

    <div class="col-md-3 col-sm-6 col-12">
    <div class="info-box">
        <span class="info-box-icon bg-success"><i class="fas fa-shopping-cart"></i></span>

        <div class="info-box-content">
        <span class="info-box-text">Orders executed</span>
        <span class="info-box-number">
            <?php echo $orderClass->productsDeliverd(); ?>
        </span>
        </div>
        <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
    </div>

    <div class="col-md-3 col-sm-6 col-12">
    <div class="info-box">
        <span class="info-box-icon bg-warning"><i class="fas fa-tags"></i></span>

        <div class="info-box-content">
        <span class="info-box-text">Coupons created</span>
        <span class="info-box-number"><?php echo ($widgetClass->allCouponsCreated()) !== false ? count($widgetClass->allCouponsCreated()) : '0'; ?></span>
        </div>
        <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-danger"><i class="fas fa-tags"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">Coupons expired</span>
            <span class="info-box-number">0</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-dark"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">Front users</span>
            <span class="info-box-number"><?php echo ($widgetClass->allActivatedFrontUsers()) !== false ? count($widgetClass->allActivatedFrontUsers()) : '0'; ?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-dark"><i class="fas fa-shopping-cart"></i></span>
            <div class="info-box-content">
            <span class="info-box-text"><a class='link text-break' href='archived.php'>Archived products</a></span>
            <span class="info-box-number"><?php echo ($widgetClass->allArchivedProducts()) !== false ? count($widgetClass->allArchivedProducts()) : '0'; ?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>