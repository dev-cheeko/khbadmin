<div class="row mb-2">
    <div class="col-sm-6">
    <h1 class="m-0 text-dark"><?php echo (isset($pageTitle)) ? $pageTitle : ""; ?></h1>
    </div><!-- /.col -->
    <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active"><?php echo (isset($pageTitle)) ? $pageTitle : ""; ?></li>
    </ol>
    </div><!-- /.col -->
</div><!-- /.row -->