<!-- Navbar -->

<?php 

$usersD = $users->getUserDataById($_SESSION['id']);

?>
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>

    </ul>

    <!-- SEARCH FORM -->
    <!-- <form class="form-inline mx-auto">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> -->

    <?php require_once "comp-Time.php"; ?>

    <!-- Right navbar links -->
    <div class="userArea">
      <a href="" class="account">
        <img src="./dist/img/avatar.png" alt="User image">
      </a>
      <span class="text-white"><?php echo $usersD->username; ?></span>
      <div class="userArea-submenu">
      <ul class="root">
        <li >
        <a href="index.php" >Dashboard</a>
        </li>
        <li >
        <a href="profile.php" >Profile</a>
        </li>
        <li >
        <a href="#">Settings</a>
        </li>
        <li>
        <a href="logout.php">Logout</a>
        </li>
      </ul>
      </div>
    </div>
    
    <ul class="navbar-nav">
      <!-- <li class="nav-item"><a class="nav-link  btn btn-primary btn-sm text-white" href="logout.php">Logout</a></li> -->
    </ul>
  </nav>
  <!-- /.navbar -->
