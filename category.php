<?php 
$pageTitle = "Category";
include "./includes/header.php"; ?>

<div class="wrapper">

<?php include "./includes/topbar.php"; ?>
  
  <?php include "./includes/sidebar.php"; 
  // <!-- Content Wrapper. Contains page content -->
  echo "<div class='content-wrapper'>";
  if(isset($_GET['type'])) {

    switch ($_GET['type']) {
      case 'sub-category':
        include_once("includes/components/Category/Comp-subcategory.php");
      break;
      case 'parent':
        include_once("includes/components/Category/Comp-parentcategory.php");
        break;
      
      default:
        # code...
        break;
    }
  } else {
    
  }
  ?>
  </div>
  
  
  <!-- /.content-wrapper -->
<?php include "./includes/footer.php"; ?>