<?php 
include "./core/init.php"; 
use Konekt\PdfInvoice\InvoicePrinter;
if(isset($_POST['orid'])) {
    $currency = 'INR ';
    $size = 'A4';
    $language = 'en';
    $invoice = new InvoicePrinter($size, $currency, $language);
    $orderID = $_POST['orid'];
    $orderData = $orderClass->orderInvoiceData($orderID);

    //  /* Header settings */
     $invoice->setLogo("dist/img/byp-default-logo.png");   //logo image path
     $invoice->setColor("#007fff");      // pdf color scheme
     $invoice->setType("Sale Invoice");    // Invoice Type
     $invoice->setReference("INV-".$orderData['orderProductdata']['order_id']);   // Reference
     $invoice->setDate(date('M dS ,Y',time()));   //Billing Date
     $invoice->setTime(date('h:i:s A',time()));   //Billing Time
    //  $invoice->setDue(date('M dS ,Y',strtotime('+3 months')));    
     // Due Date
     $invoice->setFrom(array("Bookyourproduct","Karbigahiya","Patna", "Bihar"));

     $invoice->setTo(
        array(
            $orderData['shippingData']['purchaser_name'],$orderData['shippingData']['ship_address'],
            $orderData['shippingData']['email'],
            $orderData['shippingData']['mobile'],
        )
    );
     
    foreach ($orderData['productData'] as $key => $prd) {
        $prdData = $productClass->productData($prd['product_id']);
        $invoice->addItem(
            $prdData['product_data']['product_name'],
            "",
            $prd['quantity'], // quantity
            0,  // vat
            $prd['price'], // price
            0,  // discount
            $prd['price']*$prd['quantity'] // total
        );
    }

     
     $invoice->addTotal("Total",$orderData['orderProductdata']['buy_amount']);
    //  $invoice->addTotal("VAT 21%",1986.6);
    //  $invoice->addTotal("Total due",11446.6,true);
     $invoice->addBadge($orderData['orderProductdata']['order_credit']);
     $invoice->addTitle("Important Notice");
     $invoice->addParagraph("No item will be replaced or refunded if you don't have the invoice with you.");
     $invoice->setFooternote("Bookyourproduct");
     $invoice->render('invoice.pdf','D'); 
     /* I => Display on browser, D => Force Download, F => local path save, S => return document as string */
}


?>