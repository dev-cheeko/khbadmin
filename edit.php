<?php 
$pageTitle = "Dashboard";
include "./includes/header.php"; ?>

<div class="wrapper">

<?php include "./includes/topbar.php"; ?>
  
  <?php include "./includes/sidebar.php"; ?>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
      <?php 
       include_once("includes/breadcrumb.php");
      ?>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <?php 
        if(isset($_GET['type'])) {
            if($_GET['type'] == 'product') {
                if(file_exists('includes/components/News/Comp-Editproduct.php')) {
                    $id = base64_decode($_GET['id']);
                    // Checking for product availiable not
                    $pCheck = $productClass->productCheckAv($id);
                    if($pCheck !== false){
                        $prdData = $pCheck;
                        require_once('includes/components/News/Comp-Editproduct.php');
                    }else {
                        echo 'No result found';
                    }
                } else {
                    echo 'Something went wrong';
                }
            }
        }
        ?>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include "./includes/footer.php"; ?>