var dt = $("#prdTable").DataTable(
    {
        "responsive":true,
        "processing":true,
        "serverSide":true,
        // "select": true,
        columnDefs: [ {
            orderable: false,
            "targets": 0
        } ],
        "ajax" : {
            url: "ajax/products-ajax.php",
            data: {action: 'getProducts'},
            type: 'post'
        },
        error: function() {
            
        }
    }
);

var dtarch = $("#archi__prdTable").DataTable(
    {
        "responsive":true,
        "processing":true,
        "serverSide":true,
        // "select": true,
        columnDefs: [ {
            orderable: false,
            "targets": 0
        } ],
        "ajax" : {
            url: "ajax/archived-ajax.php",
            data: {archiaction: 'getProducts'},
            type: 'post'
        },
        error: function() {
            
        }
    }
);

var dtor = $("#ordTable").DataTable(
    {
        "responsive":true,
        "processing":true,
        "serverSide":true,
        // "select": true,
        columnDefs: [ {
            orderable: false,
            // className: 'select-checkbox',
            // targets:   0
        } ],
        "ajax" : {
            url: "ajax/orders-ajax.php",
            data: {action: 'getOrders'},
            type: 'post'
        },
        error: function() {
            
        }
    }
);

tinymce.init({
    selector: 'textarea#product_des',
    plugins: [
        'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
        'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
        'table emoticons template paste help'
    ],
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
    images_upload_url: 'imgUpload.php',
    images_upload_credentials: true
});


// Color picker 
$('#prd_color').colorpicker();

// Timer
var datetime = null,
        date = null;

var update = function () {
    date = moment(new Date())
    datetime.html(date.format('dddd, MMMM Do YYYY, h:mm:ss a'));
};

$(document).ready(function(){
    datetime = $('.timer')
    update();
    setInterval(update, 1000);
});

// Changing status
function prdStatus(id) {
    var data =  id;
    $.ajax({
        type: "post",
        url: "ajax/prd-status-ajax.php",
        data: {prd_statusid_: id},
        dataType: "JSON",
        beforeSend: function() {
            toastr.info('Please wait..' , 'Info');
        },
        success: function (response) {
           if(response.status == 200 ) {
            dt.ajax.reload(null , false);
            dtarch.ajax.reload(null , false);
            toastr.success(`${response.msg}` , 'Success');
           }
           if(response.status == 400 ) {
            dt.ajax.reload(null , false);
            dtarch.ajax.reload(null , false);
            toastr.error(`${response.msg}` , 'Error');
           }
        }
    });

}

// Deleting PRD
function prdDel(i) {
    var data = i;
    $.ajax({
        type: "post",
        url: "ajax/prd-del-ajax.php",
        data: {prd_delid_: data},
        dataType: "JSON",
        beforeSend: function() {
            toastr.info('Please wait..' , 'Info');
        },
        success: function (response) {
           if(response.status == 200 ) {
            dt.ajax.reload(null , false);
            dtarch.ajax.reload(null , false);
            toastr.success(`${response.msg}` , 'Success');
           }
           if(response.status == 400 ) {
            dt.ajax.reload(null , false);
            dtarch.ajax.reload(null , false);
            toastr.error(`${response.msg}` , 'Error');
           }
        }
    });
}

function prdData(id) {
    var i = id;

    $.ajax({
        type: "post",
        url: "ajax/prd-data-ajax.php",
        data: {prd_i_: i },
        dataType: "JSON",
        beforeSend: function() {
            $('#prdDataModal').modal('toggle');
            $("#prdata").html("Please wait...");
        },
        success: function (response) {
           if(response.status == 200 ) {
            
            $("#prdata").html(`${response.msg}`);
           }
           if(response.status == 400 ) {
           
            toastr.error(`${response.msg}` , 'Error');
           }
        }
    }); 

}

$(document).on('change' , "#ordst" , function(el){
    var selVal =  $(el.target).val();
    var orid = $(el.target).data('orid');
    $.ajax({
        type: "post",
        url: "ajax/orders-ajax.php",
        data: {
            orid: orid ,  
            status: selVal
        },
        dataType: "JSON",
        beforeSend: function() {
            toastr.info('Please wait..' , 'Info');
        },
        success: function (response) {
           if(response.status == 200 ) {
            dtor.ajax.reload(null , false);
            toastr.success(`${response.msg}` , 'Success');
           }
           if(response.status == 400 ) {
            dtor.ajax.reload(null , false);
            toastr.error(`${response.msg}` , 'Error');
           }
        }
    }); 
});


function orderdet(oid) {
    var orderid = oid;
    $.ajax({
        type: "post",
        url: "ajax/ord-det-ajax.php",
        data: {orderid: orderid },
        dataType: "JSON",
        beforeSend: function() {
            $('#ordM').modal('toggle');
            $("#orddata").html("Please wait...");
        },
        success: function (response) {
           if(response.status == 200 ) {
            
            $("#orddata").html(`${response.msg}`);
           }
           if(response.status == 400 ) {
           
            $("#orddata").html(`${response.msg}` , 'Error');
           }
        }
    }); 
}


// Multi delete prd
$(".delPrdO").on("click" , function (e){
    var deleteids_arr = [];
    // Read all checked checkboxes
    $("input:checkbox[class=prddelete_check]:checked").each(function () {
       deleteids_arr.push($(this).val());
    });

    // Check checkbox checked or not
    if(deleteids_arr.length > 0){

       // Confirm alert
       var confirmdelete = confirm("Do you really want to Delete records?");
       if (confirmdelete == true) {
          $.ajax({
             url: 'ajax/prd-del-ajax.php',
             type: 'post',
             data: {request: 2, deleteids_arr: deleteids_arr},
             dataType: "JSON",
             success: function(response){
                if(response.status == 200 ) {
                    dt.ajax.reload(null , false);
                    toastr.success(`${response.msg}` , 'Success');
                }

                if(response.status == 400 ) {
                    dt.ajax.reload(null , false);
                    toastr.error(`${response.msg}` , 'Error');
                }
             }
          });
       } 
    } else{
        alert('Please select products!');
    }
});

 // Check all 
 $('#prd_checkall').click(function(){
    if($(this).is(':checked')){
       $('.prddelete_check').prop('checked', true);
    }else{
       $('.prddelete_check').prop('checked', false);
    }
 });

// Product checkbox checked
function prdcheckbox(){

    // Total checkboxes
    var length = $('.prddelete_check').length;

    // Total checked checkboxes
    var totalchecked = 0;
    
    $('.prddelete_check').each(function(){
       if($(this).is(':checked')){
          totalchecked+=1;
       }
    });
    
    // Checked unchecked checkbox
    if(totalchecked == length){
       $("#prd_checkall").prop('checked', true);
    }else{
       $('#prd_checkall').prop('checked', false);
    }
};


// Multi restore prd
$(".resorePrd").on("click" , function (e){
    var restore_arr = [];
    // Read all checked checkboxes
    $("input:checkbox[class=prdrestore_check]:checked").each(function () {
       restore_arr.push($(this).val());
    });

    // Check checkbox checked or not
    if(restore_arr.length > 0){
       // Confirm alert
       var confirmdelete = confirm("Do you really want to restore records?");
       if (confirmdelete == true) {
          $.ajax({
             url: 'ajax/prd-restore-ajax.php',
             type: 'post',
             data: {request: 3, restore_arr:  restore_arr},
             dataType: "JSON",
             success: function(response){
                if(response.status == 200 ) {
                    dtarch.ajax.reload(null , false);
                    toastr.success(`${response.msg}` , 'Success');
                }

                if(response.status == 400 ) {
                    dtarch.ajax.reload(null , false);
                    toastr.error(`${response.msg}` , 'Error');
                }
             }
          });
       } 
    } else{
        alert('Please select products!');
    }
});

 // Check all 
 $('#prdarchi_checkall').click(function(){
    if($(this).is(':checked')){
       $('.prdrestore_check').prop('checked', true);
    }else{
       $('.prdrestore_check').prop('checked', false);
    }
 });

// Product checkbox checked
function prdrestorebox(){

    // Total checkboxes
    var length = $('.prdrestore_check').length;

    // Total checked checkboxes
    var totalchecked = 0;
    
    $('.prdrestore_check').each(function(){
       if($(this).is(':checked')){
          totalchecked+=1;
       }
    });
    
    // Checked unchecked checkbox
    if(totalchecked == length){
       $("#prdarchi_checkall").prop('checked', true);
    }else{
       $('#prdarchi_checkall').prop('checked', false);
    }
};