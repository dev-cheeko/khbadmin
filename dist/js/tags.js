
var data = [
    "Electronics",
    "Gadgets",
    "CCTV",
    "Camera",
    "Security",
    "Home Appliances",
    "Kitchen",
    "tv"
];

var tags = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('tag_name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: $.map(data, function (city) {
        return {
            tag_name: city
        };
    })
});

tags.initialize();

$("#prd_tags").tagsinput({
    typeaheadjs: [{
        minLength: 1,
        highlight: true,
        limit: 10,
    },{
        minlength: 1,
        tag_name: 'tag_name',
        displayKey: 'tag_name',
        valueKey: 'tag_name',
        source: tags.ttAdapter()
    }],
    freeInput: true
});