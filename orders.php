<?php 
$pageTitle = "Orders";
include "./includes/header.php"; ?>

<div class="wrapper">

<?php include "./includes/topbar.php"; ?>
  
  <?php include "./includes/sidebar.php"; ?>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
      <?php 
       include_once("includes/breadcrumb.php");
      ?>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
      <div class="row">
            <div class="col-12">
              
               <div class="card">
                   <div class="card-header">
                       <div class="d-flex justify-content-between">
                         <span>Orders</span>
                       </div>
                   </div>
                <div class="card-body">
                   <table style="width: 100%; font-size: .9rem;" class="table  table-striped table-condensed dataTable" id="ordTable">
                        <thead>
                            <tr class="t-header">
                              <th>#</th>
                              <th>Customer ID </th>
                              <th>Order ID</th>
                              <th>Txn Id</th>
                              <th>Order status</th>
                              <th>Order credit</th>
                              <th>Pay type</th>
                              <th>Buy amount</th>
                              <!-- <th>Purchase time</th> -->
                              <th>Actions</th>
                            </tr>
                            <div class="ortr"></div>
                        </thead>
                    </table>
                </div>
               </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- Modal -->

  <div class="modal fade " id="ordM" tabindex="-1" role="dialog" aria-labelledby="orderdata" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
          <div id="orddata" class="row"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.content-wrapper -->
<?php include "./includes/footer.php"; ?>