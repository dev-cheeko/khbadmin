<?php 
// responsible for product delete change 
include_once("../core/init.php");
if( isset($_POST['prd_delid_']) ) {
    $id = $_POST['prd_delid_'];
    if(is_numeric($id) || is_int($id)) {
        $statusDel = $productClass->delproduct( $id );
        if($statusDel == true) {
            echo json_encode(array("status" => 200 , "msg"=>'Product deleted'));
        } else {
            echo json_encode(array("status" => 400 , "msg"=>'Something went wrong'));
        }
    } 
}

// Delete selected row
if(isset($_POST['request']) && $_POST['request'] == 2 ) {
    $deleteids_arr = $_POST['deleteids_arr'];
    foreach($deleteids_arr as $deleteid){
        $statusDel = $productClass->delproduct( $deleteid );
    }
    if($statusDel == true) {
        echo json_encode(array("status" => 200 , "msg"=>'Product deleted'));
    } else {
        echo json_encode(array("status" => 400 , "msg"=>'Something went wrong'));
    }
  
}

?>