<?php 
// responsible for product status change 
include_once("../core/init.php");

if( isset($_POST['prd_statusid_']) ) {
    $id = $_POST['prd_statusid_'];
    
    if(is_numeric($id) || is_int($id)) {
        $statusChange = $productClass->statusChange($id);
        if($statusChange == 'activated') {
            echo json_encode(array("status" => 200 , "msg"=>'News is now live'));
        }
        if($statusChange == 'deactivated') {
            echo json_encode(array("status" => 200 , "msg"=>'News is deactivated'));
        }
    } 
}

?>