<?php 
// responsible for product delete change 
include_once("../core/init.php");
if( isset($_POST['prd_i_']) ) {
    $id = $_POST['prd_i_'];
    if(is_numeric($id) || is_int($id)) {
        $data = $productClass->productData( $id );
        if($data == true) {
        $output = "<div class='col-md-8'><caption>Product detail</caption><table class='table table-condensed table-hover table-bordered table-striped'>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Category</th>
              <th>Retail price</th>
              <th>Offer price</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>#</td>
              <td>".$data['product_data']['product_name']."</td>
              <td>".$data['categorydata']->category_name."</td>
              <td>".$data['product_data']['retail_price']."</td>
              <td>".$data['product_data']['offer_discount']."</td>
            </tr>  
          </tbody>
        </table></div>
        <div class='col-md-4'>
          <img src='".$data['product_data']['product_imgUrl']."' alt='".$data['product_data']['product_name']."' class='img-thumbnail img-fluid'>  
        </div>
        ";
        echo json_encode(array("status" => 200 , "msg"=>$output));
      } else {
        echo json_encode(array("status" => 400 , "msg"=>'Something went wrong'));
      }
    } 
}

?>