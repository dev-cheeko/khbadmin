<?php 
// responsible for product delete change 
include_once("../core/init.php");

// restore selected row
if(isset($_POST['request']) && $_POST['request'] == 3 ) {
    $restore_arr = $_POST['restore_arr'];

    foreach($restore_arr as $restoreid){
        $statusDel = $productClass->restoreProduct( $restoreid );
    }
    if($statusDel == true) {
        echo json_encode(array("status" => 200 , "msg"=>'Product restoredd'));
    } else {
        echo json_encode(array("status" => 400 , "msg"=>'Something went wrong'));
    }
  
}

?>