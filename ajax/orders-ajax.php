<?php 
  include_once("../core/init.php");

  if(isset($_POST['action']) && !empty($_POST['action'])) {

    $action       = $_POST['action'];
    $allProducts  = $orderClass->OrdersDatatable();
    echo json_encode($allProducts);
    
  }


  if(isset($_POST['orid']) && !empty($_POST['orid'])) {
    
    $orderid      = $_POST['orid'];
    $orderStatus  = $_POST['status'];

    $statusChange = $orderClass->statusChange( $orderid , $orderStatus );
    
    if( $statusChange != false ) 
    {
      echo json_encode(array(
        'status' => 200,
        'msg'    => 'Order status changed' 
      ));
    } else {
      echo json_encode(array(
        'status' => 200,
        'msg'    => 'Order status change failed' 
      ));
    }

  }
?>

