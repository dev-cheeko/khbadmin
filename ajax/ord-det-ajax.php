<?php 
  include_once("../core/init.php");

  if(isset($_POST['orderid']) && !empty($_POST['orderid'])) {  
    $output ="";
    $output .= "<div class='col-md-12'>
    <div class='card-header bg-light'><caption>Order product detail</caption></div>
    <table class='table table-condensed table-hover table-bordered table-striped'>
    <thead>
      <tr>
        <th>#</th>
        <th>Img</th>
        <th>Name</th>
        <th>Category</th>
        <th>Price</th>
        <th>Quantity</th>
        <th>Purchase time</th>
      </tr>
    </thead>
    <tbody>";
    
    $oid = $_POST['orderid'];
    $orderProductDetails = $orderClass->OrderDetailsWithProductsOrders( $oid );

    // Shipping address
    $shippingAddress = $orderClass->shippingAddress($oid);
  
    // print_r($orderProductDetails);
    if($orderProductDetails != false) {

      foreach ( $orderProductDetails as $key => $details ) {
        $prd = $productClass->productData($details['product_id']);
          $output .= "<tr>
          <td>#</td>";
          $output .= "
          <td><img class='img-fluid img-thumbnail' width='80' src=".$prd['product_data']['product_imgUrl']."></td>
          <td><a href='".URLROOT."product.php?id=".$prd['product_data']['product_id']."&slug=".$prd['product_data']['slug']."'>".$prd['product_data']['product_name']."</a></td>
          <td>".$prd['categorydata']->category_name."</td>
          <td>".$details['price']."</td>
          <td>".$details['quantity']."</td>
          <td>".$details['ts']."</td>
          </tr>";
      }
      

      $output .= "</table></div>";

      $output .= "
      <div class='col-md-12'><div class='card-header bg-light'>Shipping address</div>
      <form disabled>
        <div class='row'>
          <div class='col-md-6'>
            <div class='form-group'>
              <label>Firstname</label>
              <input class='form-control' value='".$shippingAddress['first_name']."'>
            </div>
          </div>
          <div class='col-md-6'>
            <div class='form-group'>
              <label>Lastname</label>
              <input class='form-control' value='".$shippingAddress['last_name']."'>
            </div>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-6'>
            <div class='form-group'>
              <label>Email</label>
              <input class='form-control' value='".$shippingAddress['email_id']."'>
            </div>
          </div>
          <div class='col-md-6'>
            <div class='form-group'>
              <label>Contact</label>
              <input class='form-control' value='".$shippingAddress['mobile']."'>
            </div>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-4'>
            <div class='form-group'>
              <label>Pincode</label>
              <input class='form-control' value='".$shippingAddress['city']."'>
            </div>
          </div>
          <div class='col-md-4'>
            <div class='form-group'>
              <label>Contact</label>
              <input class='form-control' value='".$shippingAddress['state']."'>
            </div>
          </div>
          <div class='col-md-4'>
            <div class='form-group'>
              <label>Contact</label>
              <input class='form-control' value='".$shippingAddress['pin_code']."'>
            </div>
          </div>
        </div>
        <div class='form-group'>
        <label>Address</label>
        <textarea col='30' row='4' class='form-control'>".$shippingAddress['street_address']."</textarea>
      </div>
      </form>
      </div>
      </div>
      ";
      
      echo json_encode( 
          array( 
          "status"=>200 , 
          "msg" => $output 
          ) 
      );
    } else {
      echo json_encode(array(
        "status"  => 400 , 
        "msg"     => "No result found"
      ));
    }
}

?>